#!/bin/bash -i

COE_CLIENT="/home/pi/coe_client"

echo ""
echo "This script will setup COE_CLIENT and its dependencies"
echo "============================================================="

echo "Setting up timezone to Seoul (Korea)..."
sudo cp /usr/share/zoneinfo/Asia/Seoul /etc/localtime

echo "Updating system package database..."
sudo apt-get -qq update > /dev/null

echo "Upgrading the system..."
echo "(This might take a while.)"
sudo apt-get -y -qq upgrade > /dev/null

echo "Installing dependencies..."
sudo apt-get -y -qq install git-core python-pip python-dev sqlite3 omxplayer supervisor unclutter > /dev/null

# Cloning or updating omxd
if [ ! -d "$HOME/omxd" ]; then
    cd $HOME
    sudo mkdir /usr/share/doc/omxd
    git clone -q https://github.com/subogero/omxd.git
fi
cd $HOME/omxd
git checkout -q -f -- .
git pull -q
killall -9 -q omxd
killall -9 -q omxplayer.bin
make clean -q
make -q
sudo make install -q

echo "Installing (Python) dependencies..."
sudo pip install -r "$COE_CLIENT/misc/requirements.txt" -q > /dev/null

echo "Increasing swap space to 500MB..."
echo "CONF_SWAPSIZE=500" > "$HOME/dphys-swapfile"
sudo cp /etc/dphys-swapfile /etc/dphys-swapfile.bak
sudo mv "$HOME/dphys-swapfile" /etc/dphys-swapfile

echo "Checking if coe_client directory exists at HOME..."
if [ ! -d "$HOME/coe_client" ]; then
    echo "Doesn't exists. Cloning it..."
    cd $HOME
    git clone -q http://www.github.com/SNUArduinoLabs/coe_client.git
fi
echo "It exists. Pulling latest changes..."
cd $HOME/coe_client
git checkout -q -f -- .
git pull -q

## Make sure we have proper framebuffer depth.
#if grep -q framebuffer_depth /boot/config.txt; then
#  sudo sed 's/^framebuffer_depth.*/framebuffer_depth=32/' -i /boot/config.txt
#else
#  echo 'framebuffer_depth=32' | sudo tee -a /boot/config.txt > /dev/null
#fi
#
## Fix frame buffer bug
#if grep -q framebuffer_ignore_alpha /boot/config.txt; then
#  sudo sed 's/^framebuffer_ignore_alpha.*/framebuffer_ignore_alpha=1/' -i /boot/config.txt
#else
#  echo 'framebuffer_ignore_alpha=1' | sudo tee -a /boot/config.txt > /dev/null
#fi

echo "Forcing to use HDMI..."
if grep -q '^hdmi_force_hotplug' /boot/config.txt; then
    sudo sed 's/^hdmi_force_hotplug.*/hdmi_force_hotplug=1/' -i /boot/config.txt
else
    echo 'hdmi_force_hotplug=1' | sudo tee -a /boot/config.txt > /dev/null
fi
if grep -q '^hdmi_drive' /boot/config.txt; then
    sudo sed 's/^hdmi_drive.*/hdmi_drive=2/' -i /boot/config.txt
else
    echo 'hdmi_drive=2' | sudo tee -a /boot/config.txt > /dev/null
fi

echo "Disabling boot messages..."
echo "dwc_otg.lpm_enable=0 console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty3 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait logo.nologo loglevel=3" | sudo tee -a /boot/cmdline.txt
sudo cp /boot/cmdline.txt /boot/cmdline.txt.bak
sudo sed 's/$/ quiet/' -i /boot/cmdline.txt

echo "Setting SSH-server to start on boot..."
sudo update-rc.d ssh defaults > /dev/null
sudo update-rc.d ssh enable > /dev/null

echo "Coping supervisord configuration files for downloader, cleanmem, and webserver..."
sudo cp -f ${COE_CLIENT}/misc/supervisor_cleanmem.conf /etc/supervisor/conf.d/
sudo cp -f ${COE_CLIENT}/misc/supervisor_downloader.conf /etc/supervisor/conf.d/
sudo cp -f ${COE_CLIENT}/misc/supervisor_webserver.conf /etc/supervisor/conf.d/
sudo service supervisor restart > /dev/null
sudo supervisorctl reread > /dev/null
sudo supervisorctl update > /dev/null

echo "Creating cron jobs...(RBpi will reboot daily at 2 AM)"
crontab -r
crontab -l > mycron
echo "@reboot sudo omxd" >> mycron
echo "@reboot unclutter -idle 0.01 -root" >> mycron
echo "00 02 * * * sudo ./home/pi/coe_client/misc/update.sh" >> mycron
crontab mycron
rm mycron

# Creating db
echo "Creating logs directory and database file..."
cd ${COE_CLIENT}/src
mkdir -p logs
python create_db.py

echo "Creating Downloads directory if it does not exists yet..."
if [ ! -d "$HOME/Downloads/" ] ; then
    mkdir $HOME/Downloads/
fi

echo "Assuming coe_client/misc/coe_client.conf as unchanged..."
cd ${COE_CLIENT}/misc
git update-index --assume-unchanged coe_client.conf

echo "Finished. Please reboot"
echo "============================================================="
echo ""
