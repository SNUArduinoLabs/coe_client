#!/bin/bash -i

COE_CLIENT="/home/pi/coe_client"

echo "Coping supervisord configuration files for player..."
echo "This might restart supervisord, in order to update it."
sudo cp -f ${COE_CLIENT}/misc/supervisor_player.conf /etc/supervisor/conf.d/
sudo service supervisor restart > /dev/null
sudo supervisorctl reread > /dev/null
sudo supervisorctl update > /dev/null

echo "Done."
echo ""
