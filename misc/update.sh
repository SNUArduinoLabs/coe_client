#!/bin/bash -i

COE_CLIENT="/home/pi/coe_client"

echo ""
echo "Stopping all coe_client processes..."
sudo supervisorctl stop all
killall python -q

#echo "Updating system package database..."
#sudo apt-get -qq update > /dev/null

#echo "Upgrading the system..."
#echo "(This might take a while.)"
#sudo apt-get -y -qq upgrade > /dev/null

echo "Removing all cached Python files..."
cd ${COE_CLIENT}
rm -f *.pyc

echo "Fetching latest update for COE_CLIENT..."
git pull -q

#echo "Fetching latest update for omxd..."
#cd $HOME/omxd
#git pull -q
#make -q
#sudo make install -q

echo "Ensuring all Python modules are installed..."
sudo pip install -r ${COE_CLIENT}/misc/requirements.txt -q

echo "Restarting all coe_client processes..."
sudo supervisorctl start all

echo ""
