# Miscellaneous

This directory contains a set of installation and configuration scrips; as well as a set of configuration files used to set up `supervisord`. The detailed list of files is:

|File name | Description|
|:---------|:-----------|
|`setup.sh` | Script to setup (for the first time) `COE_CLIENT` system|
|`update.sh`| Script to update `COE_CLIENT` system|
|`requirements.txt`|Python module(s) requirements|
|`supervisor_downloader.conf`|Configuration file used by `supervisor` daemon for `downloader.py`|
|`supervisor_player.conf`|Configuration file used by `supervisor` daemon for `player.py`|
|`supervisor_webapp.conf`|Configuration file used by `supervisor` daemon for `webserver.py`|
|`supervisor_cleanmem.conf`|Configuration file used by `supervisor` daemon for `clean_memory.py`|

# Additional information

## Set up a RBpi
1. Download and copy an image file for the Raspbian OS. It can be found [here](https://www.raspberrypi.org/downloads/raspbian/)
2. Copy this image file into an SD card using `dd`. See the section "[copying a img file into an SD card](copying-a-img-file-in-to-an-sd-card)".
3. Insert the micro SD card into the RBpi and start the system.
4. Once logged in (default username is `pi` and password `raspberry`) run `sudo raspi-config`.
    - Expand the file system
    ![Expand file system example](http://i.imgur.com/3rYqICq.png)
    - Increase the memory given to the GPU. Go to `Advanced Options` and choose `Memory split`. Here change the memory split number for `256`.
    ![example memory split](http://i.imgur.com/BkzctJB.png)
    - If there are visible black pixels around the border or the image is too big for the current display disable/enable `overscan`.
    - If you want to change the password or host name it can be done here too.
5. Reboot the RBpi.

## Creating pair of private and public keys
Example:
```
pi@raspberrypi ~ $ ssh-keygen -t rsa -b 2049

Generating public/private rsa key pair.
Enter file in which to save the key (/home/pi/.ssh/id_rsa): <ENTER>
Created directory '/home/pi/.ssh'.
Enter passphrase (empty for no passphrase): <ENTER>
Enter same passphrase again: <ENTER>
Your identification has been saved in /home/pi/.ssh/id_rsa.
Your public key has been saved in /home/pi/.ssh/id_rsa.pub.
The key fingerprint is:
f5:1f:3d:7c:80:46:f7:56:d3:73:42:e5:b6:2f:a3:b8 pi@raspberrypi
The key's randomart image is:
+--[ RSA 2049]----+
|            ..oo+|
|           . o.+=|
|          . o ..B|
|         . o  .=.|
|        S   . .+o|
|             . .+|
|              + .|
|           . . o |
|          E..    |
+-----------------+

pi@raspberrypi ~ $ ssh-copy-id cacyss0807@192.168.1.199

The authenticity of host '192.168.1.199 (192.168.1.199)' can't be established.
ECDSA key fingerprint is f6:f2:ca:cc:5e:cc:02:96:a0:79:74:67:38:07:03:43.
Are you sure you want to continue connecting (yes/no)? yes

Warning: Permanently added '192.168.1.199' (ECDSA) to the list of known hosts.
cacyss0807@192.168.1.199's password:
Now try logging into the machine, with "ssh 'cacyss0807@192.168.1.199'", and check in:

 ~/.ssh/authorized_keys

to make sure we haven't added extra keys that you weren't expecting.
```
## Adding Network Details
> Taken from [raspberry.org](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md)

1. Open the wpa-supplicant configuration file in nano:

    `sudo nano /etc/wpa_supplicant/wpa_supplicant.conf`

2. Go to the bottom of the file and add the following:

    ```bash
    network={
        ssid="The_ESSID_from_earlier"
        psk="Your_wifi_password"
    }
    ```
    
    In the case of the example network, we would enter:

    ```bash
    network={
        ssid="testing"
        psk="testingPassword"
    }
    ```

3. Now save the file by pressing `ctrl+x` then y, then finally press enter.

    At this point, wpa-supplicant will normally notice a change has occurred within a few seconds, and it will try and connect to the network. If it does not, either manually restart the interface with `sudo ifdown wlan0` and `sudo ifup wlan0`, or reboot your Raspberry Pi with `sudo reboot`.

    You can verify if it has successfully connected using `ifconfig wlan0`. If the `inet addr` field has an address beside it, the Pi has connected to the network. If not, check your password and ESSID are correct.

## How to set up `supervisord`:
1. Download it: `sudo apt-get install supervisor`
2. Make sure it is running: `sudo supervisord restart`
3. Copy the script(s) configuration files under `/etc/supervisor/conf.d/`
> In our case we use the `.conf` files stored at `coe_client/misc/`
4. Let `supervisord` know that new files have been added by executing the following commands:
    - `sudo supervisorctl reread`
    - `sudo supervisorctl update`

That's it. Now all scripts' configuration files copied to the `conf.d` directory will be running.

We can check their status by doing:
```bash
sudo supervisorctl
# Some running/stopped/starting programs

supervisor> help # List of supported commands

default commands (type help ):
=====================================
add    clear  fg        open  quit    remove  restart   start   stop  update
avail  exit   maintail  pid   reload  reread  shutdown  status  tail  version
```

For more information visit this [link](https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-supervisor-on-ubuntu-and-debian-vps).

## Copying a `.img` file in to an SD card
1. Check the SD card name under by running the command: `df -h`. This will generate a list of devices.
2. `umount` **all** the partitions for the SD card.
3. Use `dd` to copy a `.img` file to the SD card (`dd bs=<block size> if=<src path> of=<dest path> `).
    - `bs` specifies the block size.
    - `if` is the source path of the `.img` file
    - `of` is the destination path
    - This might take a while (aporx. 400 seconds)
4. Syncronize the `dd` copy to avoid errors. (`sudo sync`)
> Note that the destination (`of=/destination/path`) for `dd` is the entire SD card and not only a partition (`/dev/sdb`).

### Example:
```
$ df -h
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda1       902G   38G  818G   5% /
none            4.0K     0  4.0K   0% /sys/fs/cgroup
udev            7.8G  8.0K  7.8G   1% /dev
tmpfs           1.6G  1.3M  1.6G   1% /run
none            5.0M     0  5.0M   0% /run/lock
none            7.9G   72M  7.8G   1% /run/shm
none            100M   76K  100M   1% /run/user
/dev/sdb1        29G   16K   29G   1% /media/cacyss0807/C3FA-F7F0

$ umount /dev/sdb1

$ dd bs=4M if=/home/cacyss0807/Desktop/2015-05-05-raspbian-wheezy.img of=/dev/sdb
781+1 records in
781+1 records out
3276800000 bytes (3.3 GB) copied, 310.081 s, 10.6 MB/s

$ sync
```

## Reducing the size of a modified `.img` file.
1. Reduce the size of the none boot partition (leave a couple of free MB)
    > This can be done using **gparted** (Linux)

    1. Select the correct SD card device and right click on the no-boot partition.
    2. From the pop-up menu select resize.

    ![gparted1](http://i.imgur.com/T55SlH6.png)

    3. Shrink its size using the slider bar.

    ![gparted2](http://i.imgur.com/rAL835K.png)
    4. Apply all changes by clicking the green check mark.
2. Create an image file from the SD card.
Follow the same process as [here](#copying-a-img-file-to-sd-card), but now the source path is the SD card device, and the destination a folder in the host computer. Here we need to limit the number of blocks to be copied. We need to copy as many bytes as `size of boot partition` + `size of files partition` occupy, we can see this information in gparted. Then we add this information to the `dd` command by adding the `count` argument. This argument limits the number of blocks to be copied, so if we selected a block of siz `1M` then we need to set the `count` argument to match the calculated size of our 2 partitions. However, if we choose the block size to be, lets say, `4M` then we need to modify the `count` argument accordinly (partition**s** size / block size).

    - Example command: `dd if=/dev/sdb of=/path/to/image bs=1M count=2048`
