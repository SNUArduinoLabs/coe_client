Web-server (``webserver.py``)
================================

Simple and lightweight Flask web-server that listen for GET request from the main server at ``/schedule`` and ``/marquee`` URLs. It also POST status and logging files to via the ``/status`` URL.

Every time the server POST something at ``/schedule``, the ``db_manager.py`` checks the action object of the JSON file, if ``action:`` add ``then:``

- If the new schedule already exists in the database, it removes its copy and re-saves its meta-data (and its playlist files' meta-data).
- If it does not exits already the ``db_manager`` adds it (with all the corresponding files entries).

On the other-hand, if ``action: del`` then the downloader proceeds to delete the given schedule from the database. Note that the physical files for that schedule don't get deleted because there might be other schedules in the client that use the same exact file. Thus, deleting physical files is a ``clean_memory.py's`` (TODO: Add link) job.

.. automodule:: webserver
    :members:
    :private-members:
