Installation
============

Currently, there are two ways to set up the client on a Raspberry Pi, a recommend set of steps that uses a pre-installed
Raspian OS image, and a Error-prone set of steps that uses shell scripts to install and configure the necessary things.

**Note:** During this document we assume that $COE_HOME points to home/coe_client as described in the set-up scripts.

Recommended Way
-----------------

1. Flash a ``coe_client`` pre-installed Raspian OS image into a micro SD card. For this, download the image
   `here <https://drive.google.com/file/d/0By3yluHw9TsuNHQ4NEJNRU9LOGM/view?usp=sharing>`_, and copy it to an micro SD card.

- If you are using some flavor of Unix use `dd` to copy it into the SD card. 
      
  Example:``dd if=/path/to/img of=/path/to/umounted/dev/sd``
    
- If you are using Windows follow the step provided in the elinux `website <http://elinux.org/RPi_Easy_SD_Card_Setup>`_.

2. Run ``sudo raspi-config`` and expand file system.

3. Follow the extra-configuration steps.


Error-prone way
----------------

1. Set up a Raspberry Pi following `this
   <https://github.com/SNUArduinoLabs/coe_client/blob/master/misc/README.md#set-up-a-rbpi>`_ instructions.

2. Download and run the ``setup.sh`` script provided inside the ``misc/`` directory.

.. code-block:: c

    curl https://raw.githubusercontent.com/SNUArduinoLabs/coe_client/master/misc/setup.sh > setup.sh
    chmod +x setup.sh
    ./setup.sh

3. Follow the extra-configuration steps.

Extra configuration
--------------------

.. warning:: 

    We recommend that the first time a Raspberry Pi is set up with an specific TV (or display) connect the TV first to the Raspberry Pi, and then proceed to turn the TV on. If the prompted image does not seem to appear correctly on the TV/Display (larger/smaller than the TV area) then run sudo raspi-config and dissable/enable overscan.

1. Edit the configuration ``$COE_HOME/misc/coe_client.conf`` file with the server's information.
- It should contain the server's ``hostname`` (URL o IP), ``username`` and port in that order.
- Example:

.. code-block:: c

    [server]
    hostname=118.91.109.214
    username=cacyss0807
    port=2000

.. note:: 

   Only modify the necessary values, changes to other parts of the configuration file might cause errors.

2. Create a pair of (empty pass-phrase) private and public keys and copy them to the remote server.

.. code-block:: c

    $ ssh-keygen -t rsa -b 2049
    $ ssh-copy-id <user>@<server url/ip>

3.Copy the ``supervisor_player.conf`` file to the supervisor ``conf.d`` directory. This can be easily done by running the ``copy_player_conf.sh`` script, placed in the misc directory.

.. code-block:: c

    $ cd $COE_HOME/misc
    $ sh copy_player_conf.sh

Or, it can be done manually, by looking at how ``supervisord`` works.

.. warning::

    It is very important to do this step at the end of the configuration process; or at least after having ssh access to the Raspberry Pi, because if the player is activated from boot there might not be way to see a prompt.
