.. COE Multimedia Broadcast System (Client) documentation master file, created by
   sphinx-quickstart on Fri Mar 11 14:32:47 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

``coe_client`` Documentation
=============================

**What is** ``coe_client`` **?** In short it is the client side of a news and multimedia system implemented for the College of Engineering at Seoul National University.


Introduction
-------------

Broadcasting systems are an effective method to convey information to a wide audience spread over a large region. The
College of Engineering (CoE) at Seoul National University utilizes the same method to promptly deliver news and recent
announcements, PR videos, and other useful information to thousands of students spread over tens of separate buildings.
The current system is based on Windows operating system and has some drawbacks.
                                                                                                            
Arduino Labs is a student Club founded at Seoul National University in December of 2012 where both Korean and
international students meet weekly to work on innovative and cross-disciplinary projects based on Arduino, Raspberry Pi
etc. Here at Arduino Labs, we strongly believe in the significance of open source and its potential to solve difficult
problems thanks to the mutual collaboration in the open-source communities. From the knowledge and expertise gained from
the past projects, Arduino Lab has been assigned the task to re-implement the current broadcasting system, overcoming

Contents:
---------

.. toctree::
   :maxdepth: 4

   installation.rst
   components.rst

Overview
---------

The `coe_client` system has three main goals:

1. Store and manage playlist schedules.
2. Download scheduled files (videos, and images) from the server.
3. Reproduce the playlists on a TV/Display connected to a Raspberry Pi.
   
Due to these three independent goals, we have divided `coe_client` in three main Python programs (`downloader.py`,
`player.py`, and `webserver,py`). The interprocess communication between these three programs is handled by a database
(`sqlite3`). Note that the database access is abstracted by the `db_manager.py`. The following image shows the
interaction between the server and client, using JSON files as medium to communicate information. Note that, the server communicates playlists' metadata via a JSON file that is `POST`(by the cliet) at the `/schedule` URL. Similarly, urgent marquee metadata is pushed to the client by the server at `/marquee` URL. Moreover, the server can pull log-data from the client by making a GET request at the`/status` URL.

  
Indices 
========

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`











