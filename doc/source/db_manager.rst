Database Manager (``db_manager.py``)
=======================================

.. autoclass:: db_manager.DatabaseManager
    :members:
    :private-members:

.. automodule:: db_manager
    :members:
    :private-members:

