Components
===========

The ``schedule``, ``file`` and urgent ``marquee`` meta-data gets transferred from the remote host (server) to the clients via a JSON file. This files is parsed by the database. Here is an example file:

1. Schedule JSON:

.. code-block:: json

    {
        "action": "add",
        "schedule_name": "sched3",
        "schedule_id": 125,
        "start_datetime": "2015-01-01 06:00:00",
        "end_datetime": "2016-12-31 18:59:59",
        "days_of_week": ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"],
        "marquee_text": "Some banner text",
        "priority": 1,
        "content_files":
        [
            {
                "filename": "1.jpg",
                "type": "IMAGE",
                "path": "/some/location",
                "playback_time": 23,
                "interval": 1,
                "play_order": 2
            }
        ]
    }

2. Urgent marquee JSON:

.. code-block:: json

    {
            "text": "some content for the marquee (한국어 잘 될까요?)",
            "start_datetime": "2015-01-01 05:01:11",
            "end_datetime": "2016-01-01 10:10:10"
    }

.. note::

    Some of the JSON file content are not used, but still are being sent. Like the playback_time.


Main Python Programs
--------------------

.. toctree::
    :maxdepth: 2

    webserver.rst
    player.rst
    downloader.rst
    db_manager.rst

Helper Modules
---------------

1. **Clean Memory (** ``clean_memory.py`` **)**

    .. automodule:: clean_memory
        :members:
        :private-members:


2. **Create Database (** ``create_db.py`` **)**

    Separate script that creates the initial schema of  ``coe_client.db`` database

    .. automodule:: create_db
        :members:
        :private-members:

3. **Log Setter (** ``log_setter.py`` **)**

    Simple and short module used to set a consistent logging format and level throughout all the modules.

    .. automodule:: log_setter
        :members:
        :private-members:


