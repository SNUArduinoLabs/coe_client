#!/usr/bin/env python
# -*- coding: utf8 -*-

import logging
import time
import os

from sqlalchemy import create_engine, Column, String, Integer, ForeignKey, Table
from sqlalchemy.ext.declarative import *
from sqlalchemy.orm import *

from log_setter import get_level, get_format

__author__ = 'Camilo Celis Guzman <cacyss0807@snu.ac.kr>'

#: Database file name.
DB_NAME = 'coe_client.db'
#: Database file relative path.
DB_PATH = 'sqlite:///db/' + DB_NAME


def create_db():
    """
    Creates a Database file, using a given schema based on ``SQLAlchemy`` and ``dataset`` database creation scheme.
    """
    base = declarative_base()
    engine = create_engine(DB_PATH)

    File_Sched = Table('file_sched', base.metadata,
                       Column('schedule_id', Integer, ForeignKey('schedule.id', ondelete='cascade'), primary_key=True),
                       Column('file_id', Integer, ForeignKey('file.id', ondelete='cascade'), primary_key=True),
                       Column('play_order', Integer, nullable=False))

    class Schedule(base):
        """
        Schedule relation.
        """
        __tablename__ = 'schedule'

        id = Column(Integer, primary_key=True)
        name = Column(String)
        start_datetime = Column(String, nullable=False)
        end_datetime = Column(String, nullable=False)
        stored_datetime = Column(String, nullable=False)
        days_of_week = Column(String, nullable=False)
        marquee_text = Column(String, default="")
        priority = Column(Integer, nullable=False)
        is_active = Column(Integer, nullable=False)
        files = relationship('File', secondary=File_Sched, backref='schedules')

    class File(base):
        """
        File relation.
        """
        __tablename__ = 'file'

        id = Column(Integer, primary_key=True)
        name = Column(String, nullable=False)
        type = Column(String)  # IMAGE, VIDEO, PDF
        interval = Column(Integer, default=0)
        location = Column(String, nullable=False)
        downloaded_yet = Column(Integer, nullable=False)

    class Marquee(base):
        """
        Marquee relation
        """
        __tablename__ = 'marquee'

        id = Column(Integer, primary_key=True)
        text = Column(String, default="")
        start_datetime = Column(String, nullable=False)
        end_datetime = Column(String, nullable=False)

    base.metadata.create_all(engine)


def create_db_folder(logger):
    """
    Checks whether the database folder (``db``), and file (``coe_client.db``) exits. If not it proceeds to create its
    folder. Otherwise, it exits.

    Args:
        logger (logging.Logger): Logger to handle messages to be logged.
    """
    curr_path = os.path.dirname(os.path.abspath(__file__))
    if not os.path.exists(curr_path + '/db'):
        logger.info("Creating database folder...")
        os.mkdir('db')
    elif os.path.exists(curr_path + '/db/' + DB_NAME):
        logger.info("Database file already exists. Finishing...")
        exit(1)

if __name__ == "__main__":
    # Setting up logger
    logging.basicConfig(format=get_format(), level=get_level())
    logger = logging.getLogger(__name__)

    # Checking for db folder and file
    create_db_folder(logger)

    start = time.time()
    create_db()
    logger.info("Database file created (%.2f ms)" % ((time.time() - start) * 1000))
