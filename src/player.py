#!/usr/bin/env python
# -*- coding: utf8 -*-
import atexit
import codecs
import logging
import os
import subprocess
import sys
import time
from datetime import datetime, timedelta
from signal import signal, SIGUSR1, SIGTERM

from db_manager import DatabaseManager
from log_setter import get_level, get_format

__author__ = 'Camilo Celis Guzman <cacyss0807@snu.ac.kr>'

#: Path to ``home`` directory.
HOME = os.path.expanduser('~/')
#: Path to ``Downloads`` directory.
DOWNLOAD_PATH = os.path.expanduser("~/Downloads/")
#: Default time to sleep to re check database
SLEEP_TIME = 1
#: Temporal marquee text file name.
MARQUEE_TEXT_FILE = 'tmp_marquee.txt'
#: Waiting time (in seconds) for the TV to enter stand by mode.
WAIT_FOR_TV_TO_STANDBY = 8

# TODO: Check why this creates a error, as if it was a local variable
tv_on = False  # TV is assumed to start off


class Player:
    """
    Plays (in an infinite loop) the current schedule's play-list. A current schedule is defined with the following
    three properties:

    1. Its play time, date, and day of the week follows within the current system date and time.

    2. It is has the highest priority among a possible set of schedules that satisfy (1).

    3. It is the newest* schedule among a possible set of schedules that satisfy both (1) and (2).

    In a nutshell the player module performs the following functions:

    - Constantly checks for a new schedule to play.

    - Constantly checks for an urgent marquee that needs to overwrite an schedule (optional) marquee text.

    - Checks for future schedules, and attempts to turn the HDMI off in case of no future nor current schedule.

    (*) Newest refers to an schedule whose date and time of **creation** is closest to current system time.
    """

    def __init__(self):
        self.db = DatabaseManager()

        logging.basicConfig(format=get_format(), level=get_level())
        self.logger = logging.getLogger(__name__)

        self.id_current_sched = None  # currently schedule name being played by player
        self.current_marquee = ""  # currently displaying marquee text
        self.db.set_active_schedules()  # set all schedule to active if they are ready to be played

        turn_tv_on()  # assume TV is off when starting player
        self._setup_omxd()  # set up omxd with correct options for omxplayer
        self._flush_omxd_playlist()  # flush any previous playlist

    def start_playing(self):
        """
        Computes the schedule that needs to be playing currently, creates a playlist using ``omxd``, and starts it.
        It also checks every ``SLEEP_WAIT`` seconds for a new schedule, if there is not, it continues playing
        the current schedule. Moreover, if there is no schedule to play it waits for ``SLEEP_WAIT`` seconds until
        checking for a new schedule again.
        """
        self.logger.debug('start_playing')

        # Starts files loop
        while True:
            # Get schedule that should be played right now and in the next five minutes
            id_sched_to_play = self.db.get_sched_to_play(curr_day=time.strftime("%a").upper(),
                                                         curr_datetime=time.strftime("%Y-%m-%d %H:%M:%S"))
            new_curr_time = (datetime.now() + timedelta(minutes=5)).strftime("%Y-%m-%d %H:%M:%S")
            next_5_min_sched = self.db.get_sched_to_play(curr_day=time.strftime("%a").upper(),
                                                         curr_datetime=new_curr_time)

            # If no schedule to play sleep for SLEEP_TIME before checking again
            # If no sched in the next 5 minutes try to turn the tv off
            if id_sched_to_play is None:
                self.id_current_sched = None  # If No schedule to play then current schedule is also None
                self.current_marquee = ""  # If No schedule to play then there is no marquee
                if next_5_min_sched is None:
                    self.logger.debug("No schedule in the next 5 minutes")
                    turn_tv_off()
                self._kill_java_processes()  # If no schedule to play remove any marquee
                self._flush_omxd_playlist()  # If no schedule to play remove any playlist
                time.sleep(SLEEP_TIME)  # Sleep some time to recheck for a possible new schedule
                continue

            # Get schedule to play marquee text, as well as possible urgent marquee text, check if there
            # is actually an urgent marquee text, which will replace the marquee text of the schedule
            marquee_text = self.db.get_sched_marquee(id_sched_to_play)
            urgent_marquee_text = self.db.get_urgent_marquee_to_play(curr_datetime=time.strftime("%Y-%m-%d %H:%M:%S"))
            if urgent_marquee_text != "":  # Check if urgent marquee text exists
                marquee_text = urgent_marquee_text

            # Update marquee text
            if self.current_marquee != marquee_text:
                self.current_marquee = marquee_text
                self.logger.debug("Using [%s] as marquee text." % self.current_marquee)
                self._display_marquee(self.current_marquee)

            # If the lastly queried schedule is different from the schedule being played.
            # Terminate current omxd playlist and start a new one with the lastly queried schedule files.
            if self.id_current_sched != id_sched_to_play:
                turn_tv_on()  # Try to turn tv on if it is off
                playlist = self.db.get_list_files(id_sched_to_play)
                self.logger.debug('Schedule: [%s]. Play list: %s' % (id_sched_to_play, playlist))

                self.id_current_sched = id_sched_to_play  # update current schedule

                self._flush_omxd_playlist()  # Add play list to omxd
                self._add_playlist(playlist)

            # If lastly queried schedule is the same as the schedule being played wait for SLEEP_TIME
            # before checking for a new schedule again.
            else:
                self.logger.debug("No NEW schedule to play. Check again in %d seconds" % SLEEP_TIME)
                time.sleep(SLEEP_TIME)

            self._check_playlist_integrity()

    def _add_playlist(self, playlist):
        """
        Adds a new playlist to ``omxd``

        Note:
            ``omxdplayer`` is a video player made for the RB Pi, and ``omxd`` is daemon that plays a playlist for
            ``omxdplayer``.

        Args:
            playlist (list of str): list of file names in order (play list)
        """
        try:
            self.logger.debug("Adding playlist: %s to omxd" % playlist)
            for fn in playlist:
                os.system("echo A " + DOWNLOAD_PATH + fn + " > /var/run/omxctl")
        except Exception, err:
            self.logger.error("Failed to add playlist to omxd")

    def _flush_omxd_playlist(self):
        """
        Completely deletes any present playlist in ``omxd``.
        """
        try:
            os.system("echo P > /var/run/omxctl")
            os.system("echo X > /var/run/omxctl")
            os.system("echo X > /var/run/omxctl")
            os.system("sudo killall -9 -q omxplayer.bin")
            os.system("sudo killall -9 -q omxplayer")
            os.system("echo X > /var/run/omxctl")
            os.system("setterm -cursor off > /dev/null 2>&1")
            os.system("setterm -blank 0 > /dev/null 2>&1")
        except Exception, err:
            self.logger.error("Failed to flush omxd playlist")

    def _setup_omxd(self):
        """
        Sets up the initial state of ``omxd``. We pass different flags to ``omxplayer``, and get ready to reproduce
        a playlist.
        """
        try:
            self._flush_omxd_playlist()
            os.system("setterm -cursor off > /dev/null 2>&1")
            os.system("setterm -blank 0 > /dev/null 2>&1")
            os.system("sudo killall -q -9 omxplayer.bin")
            os.system("sudo killall -q -9 omxplayer")
            os.system("echo O > /var/run/omxctl")  # Remove all previous options
            os.system("echo l > /var/run/omxctl") # Audio output to audio jack (fast transition)
            # os.system("omxd h")                           # use hdmi audio output (longer gap between videos)
            os.system("echo O -b > /var/run/omxctl")  # Set black background
            os.system("echo O -w > /var/run/omxctl")  # Hw audio decoding
            os.system("echo O --no-osd > /var/run/omxctl")  # Don't show status information on screen
            # The following option causes great delays b/w videos, It readjusts the screen resolution to match the video
            #os.system("echo O -r > /var/run/omxctl")      # match videos refresh rate
        except Exception, err:
            self.logger.error("Failed to start omxd daemon")

    def _display_marquee(self, marquee_text=""):
        """
        Kills any running Java process and starts a new one using the passed marquee text.

        Note:
            Displaying a marquee text using the frame buffer is being handle by an external Java program. Thus this
            method only *calls* this Java program, and passes to it the correct marquee text.

        Args:
            marquee_text (str): marquee text to display

        Note:
            If the ``marquee_text`` string is empty then it does NOT start any new java process.
        """
        # Try to kill possible existing java process
        self._kill_java_processes()

        # Only if there is some text display the marquee
        if marquee_text != "":
            # Save marquee to file
            with codecs.open(MARQUEE_TEXT_FILE, 'w', 'utf-8') as temp:
                temp.write(marquee_text)

            self.logger.debug("Starting DisplayMarquee.jar with %s as text" % marquee_text)
            # java -Dcom.sun.javafx.transparentFramebuffer=true -jar DisplayMarquee.jar "Marquee text"
            try:
                subprocess.Popen(["java", "-Dfile.encoding=UTF-8",
                                  "-Dcom.sun.javafx.transparentFramebuffer=true",
                                  "-jar", "DisplayMarquee.jar"], stdout=subprocess.PIPE)
                # os.system("unclutter -idle 0.01 -root")  # Disable mouse pointer # Not working
                os.system("setterm -cursor off")
                os.system("setterm -blank 0")
            except Exception, err:
                self.logger.error("Error trying to display a new marquee [%s]. Details: %s" % (marquee_text, err))

    def _kill_java_processes(self):
        """
        Kills **any** Java process currently running.
        :rtype: object
        """
        try:
            self.logger.debug("Trying to kill all instances of JAVA")
            os.system("killall -q -9 java")
        except Exception, err:
            self.logger.error("Error trying to kill Java. Details: " % err)

    @staticmethod
    def _check_playlist_integrity():
        """
        Checks if a file has failed to exit ``omxplayer``. This means that ``omxplayer`` is trying to reproduce it for
        longer than its video length. This can be check using ``omxd S`` command, and based on that we can skip the to the
        next file using ``omxd n``.
        """
        # Check if a file has failed and it is reproducing over its video length and skip to next file
        # note that omxd S outputs is of the form: "Playing X/Y <FILE PATH>"
        try:
            omxds_output = subprocess.Popen(["omxd", "S"], stdout=subprocess.PIPE).communicate()[0]  # get omxd S output
            elapsed_playback_time = (omxds_output.split(" ")[1]).split("/")[0]  # parse first int
            video_playback_time = (omxds_output.split(" ")[1]).split("/")[1]  # parse second int
        except Exception, err:
            elapsed_playback_time = 0  # default values
            video_playback_time = 0

        # If the lapsed time since we started playing the video surpasses the actual video length then skip it.
        if int(elapsed_playback_time) > int(video_playback_time):
            subprocess.Popen(["echo", "n", ">", "/var/run/omxctl", ">", "/dev/null", "2>&1"])
            #subprocess.Popen(["omxd", "n"], stdout=subprocess.PIPE).communicate()[0]


########################################################################################################################
# Utilities function(s)


def turn_tv_on():
    """
    Turns the HDMI output ON, and resets frame-buffer.
    """
    os.system('sudo tvservice -p > /dev/null 2>&1; fbset -depth 8; fbset -depth 16')

def turn_tv_off():
    """
    Turns the HDMI output OFF.
    """
    os.system('sudo tvservice -o > /dev/null 2>&1')
    time.sleep(WAIT_FOR_TV_TO_STANDBY)  # Wait for the TV to go to stand by mode

@atexit.register
def exit_gracefully():
    """
    Kills every instance of the ``omxplayer``, as well as any Java process, flushes any existing ``omxd`` playlist,and
    finally exists.

    Note:
        This graceful exist is needed in case of a sudden exit of the program, in order to re-start it without any
        conflicts.
    """
    os.system("sudo killall -9 omxplayer.bin")
    os.system("sudo killall -9 omxplayer")
    os.system("sudo killall -9 java")
    os.system("echo P > /var/run/omxctl")
    os.system("echo X > /var/run/omxctl")
    sys.exit()


# Signal given by supervisord when stopped
def signal_handler(signum, frame):
    """
    Catches signal interrupts sleep() calls, so the currently playing web or image assets are skipped. It calls for an
    graceful exit.
    """
    logging.debug('USR1 received, skipping.')
    exit_gracefully()


########################################################################################################################
if __name__ == "__main__":
    logging.basicConfig(format=get_format(), level=get_level())
    signal(SIGUSR1, signal_handler)
    signal(SIGTERM, signal_handler)
    player = Player()
    player.start_playing()
