#!/usr/bin/env python
# -*- coding: utf8 -*-

import hashlib
import time
import os
import logging
import ConfigParser
from datetime import datetime, timedelta

import paramiko

from scp import SCPClient, SCPException

from log_setter import get_format, get_level
from db_manager import DatabaseManager

__author__ = 'Camilo Celis Guzman <cacyss0807@snu.ac.kr>'

#: Default ``port`` for SCP & SSH connection.
DEF_PORT = "22"
#: Default ``hostname`` for SCP & SSH connection.
DEF_HOSTNAME = '192.168.0.35'
#: Default ``username`` for SCP & SSH connection.
DEF_USERNAME = 'test'

#: SCP & SSH connection configuration file relative path.
CONF_FILE = '../misc/coe_client.conf'
#: Time in minutes of longest time allowed TODO
DELTA_RE_CHECK_CONF = 10

#: RSA Key file path.
RSA_KEY_PATH = os.path.expanduser("~/.ssh/id_rsa")
#: SSH connection time out default in seconds.
DEF_SSH_TIMEOUT = 10  # In seconds

#: Path to ``Downloads`` directory.
DOWNLOAD_PATH = os.path.expanduser("~/Downloads/")
#: Waiting  time for re-try in case of connection error.
WAIT_TIME_CONN_ERR = 30  # In seconds
#: Waiting time for re-check database, in search of new files to download.
WAIT_RECHECK_DB = 30  # In seconds
#: Maximun number of times to try to re-download a file which failed to download.
MAX_DOWNLOAD_TRIES = 5

#: Flag to identify **not yet** downloaded files.
NOT_DOWNLOADED = 0
#: Flag to identify **already** downloaded files.
DOWNLOADED = 1
#: Flag to identify files that are **being downloaded.**
DOWNLOADING = 2


class Downloader:
    """
    Tries to download all the files needed to be download based on the schedules' metadata. It connects
    to a remote machine via SSH, and downloads the files using SCP.

    - It tries to download all the files whose flag ``downloaded_yet`` corresponds to not downloaded yet files (0). \
    Also, it keeps track of all files that failed to download, and re-tries to download them x(``MAX_DOWNLOAD_TRIES``: \
    5) times more. If a file fails to download more than that x number of times its entry gets deleted from the database.

    - It sets a schedule's flag ``is_active`` to True (1) only when all its files have been downloaded.

        - If a schedule's files have failed more than x number of times, since the entries for those files that failed \
        will get deleted from the database, eventually all files for a given schedule will have been downloaded \
        successfully.

        - If all the files of a schedule fail to download it will have no files entries, therefore downloader tries to \
        remove all schedules with no files.

    Note:
        - The ``downloaded_yet`` flag, is a ``File`` relationship row which indicates one of 3 states: downloaded, \
        downloading, and not downloaded.
        - The ``is_active`` flag, is a ``Schedule`` relationship row which indicates if an schedule can be played or \
        not.
    """
    def __init__(self):
        """
        Sets the port, hostname, username, logger, private key file path, and database manager object
        """
        # Setting module's logger and get file handler from project logger_setter
        logging.basicConfig(format=get_format(), level=get_level())
        self.logger = logging.getLogger(__name__)

        # Setting paramiko module logger to only log in WARN
        logging.getLogger("paramiko.transport").setLevel(logging.WARN)
        logging.getLogger("scp").setLevel(logging.WARN)

        self.hostname, self.username, self.port = get_server_conf(CONF_FILE, DEF_HOSTNAME, DEF_USERNAME, DEF_PORT)
        self.logger.info("Using hostname (%s), port (%s), username (%s)." % (self.hostname, self.port, self.username))

        self.key_file = RSA_KEY_PATH
        self.db = DatabaseManager()

    class ConnectionError(Exception):
        """
        Connection error exception. Used internally to catch host connection errors
        """
        pass

    def _connect_to_host(self):
        """
        Connects to the host via SSH. It does so using class variables to indicate the hosts' IP, user and port.
        It also checks the latest time that the configuration file was modified for the host parameters. If it was
        within ``DELTA_RE_CHECK_CONF`` it re-loads the server configuration by reading the file.

        Raise:
            ConnectionError

        Returns:
             ssh (SSHClient): SSH Client object.

        Returns:
             scp (SCPClient): SCP Client object.
        """

        tmtime = os.path.getmtime(CONF_FILE)
        modified_time = datetime.fromtimestamp(tmtime)

        if modified_time > (datetime.now() - timedelta(minutes=DELTA_RE_CHECK_CONF)):
            self.logger.info("Re-reading configuration file for server...")
            self.hostname, self.username, self.port = get_server_conf(CONF_FILE, DEF_HOSTNAME, DEF_USERNAME, DEF_PORT)

        self.logger.debug("Using hostname (%s), port (%s), username (%s)." % (self.hostname, self.port, self.username))

        try:
            private_key = paramiko.RSAKey.from_private_key_file(self.key_file)
        except Exception, err:
            self.logger.error("Failed to get private key. Details: %s" % err)
            raise self.ConnectionError

        # Try to get private key given file path
        self.logger.debug("Trying to connect to host, and Creating an SSH client...")
        start_time = time.time()
        ssh = paramiko.SSHClient()

        try:
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=self.hostname, port=self.port, username=self.username,
                        pkey=private_key, timeout=DEF_SSH_TIMEOUT)
        except Exception, err:
            self.logger.error("Failed to connect to host. Details: %s" % err)
            ssh.close()
            raise self.ConnectionError

        self.logger.info("Successfully stabilised connection with remote host (%.2f sec)" % (time.time() - start_time))
        self.logger.debug("Creating SCP client using SSH client...")

        try:
            scp = SCPClient(ssh.get_transport())
        except Exception, err:
            self.logger.error("Failed to create SCP client. Details: %s" % err)
            raise self.ConnectionError

        self.logger.info("Successfully created SCP client.")
        return scp, ssh

    def download_files(self):
        """
        Checks the database and gets a list of all files that have NOT been downloaded yet. Try to download those.
        If any of those fail keep track how many times them failed to download, keep trying to download this set of
        failed-to-download-files until some reaching MAX_DOWNLOAD_TRIES times, by then just removes this file's entry
        from the database.
        """
        # {'filename': 'number of failed times'}
        map_failed_files = dict()  # Maps a file name with the number of times it has failed to download
        while True:
            self.logger.debug("Getting list of files to download...")
            files_to_download_metadata = self.db.get_files_to_download()
            self.logger.debug("Files to download: %s" % ([f['name'] for f in files_to_download_metadata]))

            if not files_to_download_metadata:
                self.logger.debug("No files to download. Waiting for %.1f" % WAIT_RECHECK_DB)
                time.sleep(WAIT_RECHECK_DB)
                continue

            # Try to connect to remote host. In case of error sleep for DEF_WAIT_TIME_CONN_ERR
            try:
                scp, ssh = self._connect_to_host()
            except self.ConnectionError:
                self.logger.debug("Waiting for %.1f seconds to try to reconnect again..." % WAIT_TIME_CONN_ERR)
                time.sleep(WAIT_TIME_CONN_ERR)
                continue

            for file_metadata in files_to_download_metadata:
                curr_file_name = file_metadata['name']
                self.db.set_downloaded_yet(curr_file_name, DOWNLOADING)
                successfully_downloaded = self._download_one_file(scp, curr_file_name, file_metadata['location'])

                if not successfully_downloaded:
                    # Check current number of times the file has failed to be downloaded. If it is more or equal to a
                    # max (MAX_DOWNLOAD_TRIES), remove its entry from database.
                    self.db.set_downloaded_yet(curr_file_name, NOT_DOWNLOADED)
                    if curr_file_name in map_failed_files:
                        curr_num_fails = map_failed_files[curr_file_name]
                    else:
                        curr_num_fails = 0

                    map_failed_files[curr_file_name] = curr_num_fails + 1

                    if curr_num_fails >= MAX_DOWNLOAD_TRIES:
                        self.logger.warning(
                            "File [%s] has reached the max. number of download tries (%d)."
                            "Its entry will get deleted from database." % (curr_file_name, MAX_DOWNLOAD_TRIES))
                        self.db.delete_file(curr_file_name)

                        # Delete entries from map_filed_files
                        del map_failed_files[curr_file_name]
                else:
                    # If the file failed to download (n times but still within the limit) remove it from failed to
                    # download files
                    if curr_file_name in map_failed_files:
                        del map_failed_files[curr_file_name]
                    self.db.set_downloaded_yet(curr_file_name, DOWNLOADED)

            scp.close()
            ssh.close()

            # If NO file failed to download then check every WAIT_RECHECK_DB for a new schedule.
            # If new schedule exists re-start downloader
            if len(map_failed_files) == 0:
                self.logger.debug("Successfully downloaded ALL files. Sleeping until database is modified...")
                self.db.delete_orphans_schedules()
                self.db.set_active_schedules()
            else:
                self.logger.warning("Successfully downloaded SOME files. List of failed files: %s."
                                    "Re-tying by queering database for files to download." % map_failed_files)

    def _download_one_file(self, scp, fn, loc):
        """
        Downloads a given file from a givn remove location at to the DOWNLOAD_PATH.
        :param scp: SCPClient connected to host
        :param fn: File name
        :param loc: File location
        :type fn: string
        :type loc: string
        :type scp: SCPClient
        :return: True if successfully downloaded the file. False otherwise
        """
        self.logger.debug("Trying to download file [%s]" % fn)
        if not os.path.exists(DOWNLOAD_PATH + fn):
            # Download the file with SCP and measure the time
            start = time.time()
            try:
                # The filename nor the path have file extension
                scp.get(remote_path=loc, local_path=DOWNLOAD_PATH)
                self.logger.info("Finish downloading file [%s] (%.2f seconds)" % (fn, (time.time() - start)))
            except SCPException, msj:
                self.logger.error("Error downloading file [%s] through scp. "
                                  "Details: %s " % (fn, msj.message.replace("scp: ", "")))
                scp.close()
                return False
        else:
            # Change the filename for its checksum:
            self.logger.debug("Running checksum check. Using SHA1")
            checksum = get_file_hash(DOWNLOAD_PATH + fn, hashlib.sha1())

            # If the filename given has its file type appended (.<filetype>), then save it use it to rename
            # the file name after calculate its checksum. If it does not have then filetype will be an empty
            # string so concatenating it to the new file name will have no effect
           
            filetype = "."  # Set the basic string for a file type (if exists)
            try:
                filetype += fn.split('.')[1]
            except IndexError:
                filetype = ""

            # Append file type to new file name (checksum) and rename the file accordingly
            checksum += filetype

            # Check if new filename is the same as the proposed filename then do nothing otherwise throw an error
            if checksum != fn:
                self.logger.error("Checksum check FAILED. Removing file, and adding to list of \'failed to download"
                                  "list' of files.")
                os.remove(DOWNLOAD_PATH + fn)
                return False
            else:
                self.logger.debug("Checksum check PASSED.")
            return True

########################################################################################################################
# Utilities function(s)

def get_file_hash(fn, hasher, blocksize=65536):
    """
    :param fn:  filename
    :param hasher: hasher method
    :param blocksize:  blocksize
    :return: a file hash using a hasher method with a blocksize
    """
    f = open(fn, 'rb')
    buff = f.read(blocksize)
    while len(buff) > 0:
        hasher.update(buff)
        buff = f.read(blocksize)
    return hasher.hexdigest()


def get_server_conf(fn, def_hostname, def_username, def_port):
    """
    Parses a configuration file and returns the server configuration options. If there is problem reading those then
    the default values are returned
    :param fn: configuration file name
    :param def_hostname: default hostname
    :param def_username: default username
    :param def_port: default port
    :return: hostname, username and port
    """
    SERVER_SECTION = 'server'
    settings = ConfigParser.ConfigParser()
    try:
        settings.read(fn)
        def_hostname = settings.get(SERVER_SECTION, 'hostname')
        def_username = settings.get(SERVER_SECTION, 'username')
        def_port = int(settings.get(SERVER_SECTION, 'port'))
    except Exception, err:
        logging.error("Error reading configuration file. Details: %s" % err)

    return def_hostname, def_username, def_port


########################################################################################################################

if __name__ == "__main__":
    # Testing downloader class
    down = Downloader()
    down.download_files()
