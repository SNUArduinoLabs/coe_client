#!/usr/bin/env python
# -*- coding: utf8 -*-

import os
import logging

from log_setter import get_level, get_format
from db_manager import DatabaseManager

__author__ = 'Camilo Celis Guzman <cacyss0807@snu.ac.kr>'

#: Absolute path to ``Downloads`` directory
DOWNLOAD_PATH = os.path.expanduser("~/Downloads/")
#: Minimum amount of memory (in percentage) necessary before put the clean memory policy in action.
MEM_THRESHOLD = 30


def do():
    """
    Main method: It checks the system's current free-space, and if it has passed a set threshold (30% of free-memory
    remaining), then it will delete the necessary (unused) file(s) to bring the free-space percentage below the
    threshold again. The order of deletion is given by the oldest files that are not being used by any current schedule.

    - It also deletes any expired schedule(s) and marquee(s) from the database.

    Warning:
        - In the case that after removing all possible files (files not being used by any schedule) the free-space \
        percentage is smaller than the threshold, it prompts an error message.

        - In the extreme case that there is no enough memory to store a single file the downloader might have problems \
        downloading files for new schedule(s). So any new schedule that tries to download files will fail and all its \
        file will get removed from the database (after trying to download them *x* times).
    """
    # Getting free space in the system
    free_space_ratio = get_free_space_ratio()
    logger.info("Currently, there is {0}% of free memory. Threshold: {1}%".format(free_space_ratio, MEM_THRESHOLD))

    if free_space_ratio <= MEM_THRESHOLD:
        db = DatabaseManager()  # Getting database manager object

        logger.info("Removing all expired schedules and marquees from database...")
        db.delete_expired_schedules()
        db.delete_expired_marquees()
        db.delete_orphan_files()

        # Listing all files used by all schedules
        files_being_used = db.get_list_all_files()

        # Get all files present in the DOWNLOAD_PATH
        all_files_metadata = dict()
        all_files = os.listdir(DOWNLOAD_PATH)

        # Get creating time for all files and sort tem by that time
        for f in all_files:
            all_files_metadata[f] = os.path.getmtime(DOWNLOAD_PATH + f)

        all_files.sort(key=(lambda filename: all_files_metadata[filename]))

        # Delete a file and check if threshold is met
        for f in all_files:
            if f not in files_being_used:
                logger.info("Removing file [%s]" % f)
                try:
                    os.remove(DOWNLOAD_PATH + f)
                except Exception, err:
                    logger.error("Error trying to delete file %s. Details: %s" % (f, err))

                free_space_ratio = get_free_space_ratio()
                if free_space_ratio > MEM_THRESHOLD:
                    break

        free_space_ratio = get_free_space_ratio()
        if free_space_ratio < MEM_THRESHOLD:
            logger.error("Removed all possible files (not used by any active schedule). "
                         "Free space: {0}% < {1}% (threshold)".format(free_space_ratio, MEM_THRESHOLD))
        else:
            logger.info("Deleted min. number of files necessary. Free-memory:{0}%".format(free_space_ratio))
    else:
        logger.info("There is still enough memory. Terminating...")


def get_free_space_ratio():
    """
    Returns:
        free_space (float): Returns the percentage of free space.
    """
    monitor = os.statvfs("/")
    space_left = monitor.f_bavail * monitor.f_frsize
    disk_space = monitor.f_blocks * monitor.f_frsize
    # Calculating the percentage of free space
    free_space = (space_left * 100) / disk_space
    return free_space

if __name__ == "__main__":
    # Setting logger
    logging.basicConfig(format=get_format(), level=get_level())
    logger = logging.getLogger(__name__)
    do()
