#webserver.py
#!/usr/bin/env python
# -*- coding: utf8 -*-

import threading
import json
import os
from time import strftime
from logging import basicConfig
from subprocess import PIPE, Popen

from flask import Flask

from flask import request

from db_manager import DatabaseManager
from log_setter import get_format, get_level

__author__ = 'Camilo Celis Guzman <cacyss0807@snu.ac.kr>'

DEFAULT_LINE_N = 5  # Default number of lines to get from logs in case of query parameter erro
TIME_OUT = 5  # Max. time out time in seconds for getting schedules info as JSON in POST# request

app = Flask(__name__)

# Debugging information, ignore otherwise
if not app.debug:
    basicConfig(format=get_format(), level=get_level())
    app.logger.setLevel(get_level())
    app.logger.info('Starting flask webapp...')

# creating instance of database object
db = DatabaseManager()

# get the JSON file from the server
@app.route('/schedule', methods=['POST'])
def get_sched():
    """
    Listen for POST request at ``/schedule`` URL. It receives a JSON file corresponding to a schedule metadata.
    It then, calls the database manager to store this metadata.

    Returns:
        String: 'Status: OK' if successfully saved metadata in the database.

    Warning:
        It logs an error if it was **not** possible to save the schedule metadata in the database
    """
    req_json = request.get_json()

    # Save info from JSON file into database
    global sched_save_thread

    try:
        sched_save_thread = threading.Thread(target=db.save_sched_to_db, name="sched_save_thread", args=(req_json,))

        app.logger.debug('%s created. Starting it.' % sched_save_thread.getName())
        sched_save_thread.start()

    except threading.ThreadError, e:
        app.logger.error('Error saving data to db from schedule JSON. Details: %s' % e)

    # Need to wait the JSON to download and store info on the db so that downloader can get correct values from db
    sched_save_thread.join(timeout=TIME_OUT)

    return '[get_sched] Status: OK'


@app.route('/marquee', methods=['POST'])
def get_marquee():
    """
    Listens for POST request at ``/marquee`` URL. It receives a JSON file corresponding to a urgent marquee
    metadata. It then, calls the database manager to store this metadata.

    Returns:
        String: 'Status: OK' if successfully saved metadata in the database.

    Warning:
        It logs an error if it was **not** possible to save the marquee metadata in the database
    """

    # getting JSON file from request
    req_json = request.get_json()

    global marquee_save_thread
    try:
        marquee_save_thread = threading.Thread(target=db.save_marquee_to_db, args=(req_json,), name='marquee_thread')

        app.logger.debug('%s created. Starting it.' % marquee_save_thread.getName())
        marquee_save_thread.start()

    except threading.ThreadError, e:
        app.logger.error('Error saving data to db from marquee JSON. %s' % e)

    marquee_save_thread.join(timeout=TIME_OUT)

    return '[get_marquee] Status: OK'


@app.route('/status', methods=['GET'])
def set_status():
    """
    Listens for GET request at the /status URL, and returns a JSON file with log information of the main
    Python modules (downloader, webserver, player), as well as the currently playing file. It expects 3 query
    arguments, which are the number of lines to retrieve from the log files.

    Raises:
        IndexError: If not enough query parameters are provided.

    Returns:
        JSON: file with part of the logs.

    Warning:
        It logs two possible error:

        1. If the query parameters format is not correct, or if are not provided at all.

        2. If it fails to communicate with ``omxd`` to retrieve currently playing file.
    """
    # try to get the query parameters, and assert that there are 3 of them
    try:
        query_param = request.args.get('numOfLines')
        list_lines = [int(e) for e in query_param.split(',')]
        if len(list_lines) < 3:
            raise IndexError('Not enough parameters')
    # in case of error set the query parameters to their default values
    except Exception, err:
        app.logger.error('Invalid query parameters. Setting default values to %s. Details: %s' % (DEFAULT_LINE_N, err))
        list_lines = [DEFAULT_LINE_N, DEFAULT_LINE_N, DEFAULT_LINE_N]

    # create a dictionary (JSON) to store all the status information
    response_dic = dict(downloading=[], playing="", logs={'player': [], 'downloader': [], 'webapp': [], 'omxd': []},
                        timestamp=strftime("%Y-%m-%d %H:%M:%S"))

    # Try and get currently playing file by calling omxd S
    try:
        p = Popen(["omxd", "S"], stdout=PIPE)
        (out, err) = p.communicate()
        playing_file = out.split('/')[-1]
    except Exception, err:
        playing_file = "Error getting playing file"
        app.logger.error("Failed to get omxd currently playing file. Details: %s" % err)

    # insert information regarding logs
    response_dic['playing'] = playing_file
    response_dic['downloading'] = [f['name'] for f in db.get_downloading_files()]  # files to be downloaded in db
    # TODO: Change omxd.log to match logging format
    response_dic['logs']['webapp'] = tail('logs/webserver-access.log', list_lines[0])
    response_dic['logs']['player'] = tail('logs/player-access.log', list_lines[1])
    response_dic['logs']['omxd'] = tail(" /var/log/omxstat", list_lines[1])
    response_dic['logs']['downloader'] = tail('logs/downloader-access.log', list_lines[2])

    # dump dictionary to a JSON file and return it to GET request
    return json.dumps(response_dic, indent=4)


def tail(fn, lines):
    """
    Opens a file and returns the file last n lines as a list

    Args:
        fn (string): Log file name.
        lines (int): Number of lines to retrieve.

    Returns:
        list of strings: last n lines of a file from its tail.

    Examples:
        Assume a file (``log.txt``), that has 10 lines of text on it.

        >>> tail(log.txt, 4)
        ["line 10", "line 9", "line 8", "line 7"]
    """
    try:
        f = open(fn, 'r')
        return _tail(f, lines)
    except IOError, err:  # file no found, or error opening
        if err.errno != 2:
            app.logger.warning("Could NOT open file %s. Details: % s" % (fn, str(err)))
        return []


def _tail(f, lines=1, buffer_size=4098):
    """
    Tails an already opened file, and returns a list of them. It does this process in batches using a buffer.

    Args:
        f (TextIOWrapper): File description.
        lines (int, optional): Number of lines to tail the file by, defaults to 1.
        buffer_size (int, optional): Buffer size to batch the tail process, defaults to 4098.

    Returns:
        list of strings: last n lines of a file from its tail.

    Example:
        Assume a file (``log.txt``) that has 10 lines of text on it.

        >>> with open(log.txt, 'r') as f:
                _tail(f)
        ["line 10"]

    """
    lines_found = []

    # block counter will be multiplied by buffer to get the block size from the end
    block_counter = -1

    # loop until we find n lines
    while len(lines_found) < lines:
        try:
            f.seek(block_counter * buffer_size, os.SEEK_END)
        except IOError:  # either file is too small, or too many lines requested
            f.seek(0)
            lines_found = f.readlines()
            break

        lines_found = f.readlines()

        # we found enough lines, get out
        if len(lines_found) > lines:
            break
        # decrement the block counter to get the next n bytes
        block_counter -= 1

    return lines_found[-lines:]


if __name__ == "__main__":
    os.system("fuser -n tcp -k 5000")
    app.run(host='0.0.0.0', port=5000, debug=False)
