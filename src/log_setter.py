#!/usr/bin/env python
# -*- coding: utf8 -*-
import logging

__author__ = 'Camilo Celis Guzman <cacyss0807@snu.ac.kr>'


def get_level():
    """
    Returns:
         Logging level (``WARN``, ``DEBUG``, ``ERROR``, etc)
    """
    return logging.WARN


def get_format():
    """
    Returns:
         String representing the format of each row in a log file.
    """
    return '%(asctime)s [%(filename)-18s: %(lineno)-3d] %(levelname)-8s - %(message)s'
