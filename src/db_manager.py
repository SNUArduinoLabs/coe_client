#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import time
import os
import logging

import dataset

from log_setter import get_format, get_level

__author__ = 'Camilo Celis Guzman <cacyss0807@snu.ac.kr>'

#: Database filename.
DB_NAME = 'coe_client.db'
DB_ABS_PATH = os.path.dirname(os.path.abspath(__file__)) + '/db/' + DB_NAME
#: Database file absolute path.
DB_PATH = 'sqlite:///' + DB_ABS_PATH
#: Default interval between of images, in seconds (used when no interval is given).
DEF_INTERVAL = 0
#: Default start and end date and time, when such information is not given. (basically makes the file irreproducible)
DEF_START_AND_END_DT  = '1990-01-01 00:00:00'

#: Flag value for not downloading files.
FLAG_NOT_DOWNLOADED = 0
#: Flag value for already downloaded files.
FLAG_DOWNLOADED = 1
#: Flag value for downloading files.
FLAG_DOWNLOADING = 2

class DatabaseManager:
    """
    Module that acts as a intermediate between the database (``coe_client.db``) and different modules by providing an
    API to query the database.
    """
    def __init__(self):
        self.db = None
        self.curr_sched_id = None

        # setting module's logger and get file handler from project logger_setter
        logging.basicConfig(format=get_format(), level=get_level())
        self.logger = logging.getLogger(__name__)

        self._connect()

    def _connect(self):
        """
        Tries to connect to the database.

        Warning:
            It logs an error if it fails to connect to the database.
        """
        try:
            self.logger.debug("Connecting to db <%s>" % DB_NAME)
            self.db = dataset.connect(DB_PATH)
        except Exception, err:
            self.logger.error("Failed to connect to db. Does %s exists?. Details: %s" % (DB_PATH, err))

    ###################################################################################################################
    # Saving JSON files to database

    def save_marquee_to_db(self, js):
        """
        Takes a marquee metadata as a JSON file, parses it and stores its information in the database.

        Args:
            js (JSON): marquee metadata JSON file.

        Warning:
            In case that there is an error parsing the JSON file it logs the error and rolls-back the database operation.
        """
        self.db.begin()
        self.logger.debug("Trying to save JSON (marquee) into the database.")
        start = time.time()

        try: # getting info from basic JSON file structure
            con = _write_unicode(js['text'])
            stm = js['start_datetime']
            etm = js['end_datetime']
        except KeyError, err:
            self.logger.error("Error parsing JSON file. This marquee will NOT be stored."
                              "Details: Error parsing %s" % err)
            self.db.rollback()
            return

        marquee_table = self.db['marquee']
        marquee_table.insert(dict(text=_read_unicode(con), start_datetime=stm, end_datetime=etm))
        self.db.commit()

        self.logger.debug("Marquee metadata has been successfully stored in the database (%.2f ms)." %
                         ((time.time() - start) * 1000))

    def save_sched_to_db(self, js):
        """
        Takes a schedule metadata as a JSON file, parses it and depending on the selected action ``action: <del/add>``,
        it either stores the information in the database,or deletes an schedule entry in the database.

        Args:
            js (JSON): schedule metadata JSON file.

        Warning:
            - In case that there is an error parsing the JSON file it logs the error and rolls-back the database \
            operation.

            - When deleting an schedule entry, it will never delete the schedule's entries from the database. This is \
            handled by a different method to remove all orphan file entries.
        """
        self.logger.info("Trying to save JSON (schedule) into the database.")

        # Getting instances of tables
        sched_table = self.db['schedule']
        file_table = self.db['file']
        file_sched = self.db['file_sched']

        self.curr_sched_id = js['schedule_id']
        self.logger.debug("Parsing JSON for [%s] schedule..." % self.curr_sched_id)

        # If currently playing/to play schedule is the one to be modified (re-added or deleted) STOP the player
        curr_datetime = time.strftime("%Y-%m-%d %H:%M:%S")

        sched_to_play = self.get_sched_to_play(curr_day=time.strftime("%a").upper(), curr_datetime=curr_datetime)
        if sched_to_play == self.curr_sched_id and self.is_sched_active(sched_to_play):            
            self.flip_sched_is_active(sched_to_play)

        # If there exists an entry for action then use it. Otherwise assume add
        action_is_add = True
        if 'action' in js:
            action_is_add = js['action'] == 'add'

        # If we need to add a new schedule to the data base
        if action_is_add:
            self.db.begin()
            self.logger.debug("Requested to save schedule [%s] in the database." % self.curr_sched_id)
            # If schedule already exists remove it and save it again
            if sched_table.count(id=self.curr_sched_id) != 0:
                self.logger.info("The schedule [%s] already exists. Removing it from database." % self.curr_sched_id)
                sched_table.delete(id=self.curr_sched_id)
                file_sched.delete(schedule_id=self.curr_sched_id)
                self.delete_orphan_files()
                self.logger.debug("Re-saving schedule [%s] to db." % self.curr_sched_id)

            # Checking if JSON's structure for schedule metadata is correct
            try:
                schedule_name = js['schedule_name']
                start_datetime = js['start_datetime']
                end_datetime = js['end_datetime']
                days_of_week = ','.join(js['days_of_week'])
                priority = js['priority']
                marquee = js['marquee_text']
            except KeyError, err:
                self.logger.error('Error parsing data from JSON for schedule [%s]. This schedule will NOT be stored. '
                                  'Details: Error parsing %s' % (self.curr_sched_id, err))
                self.db.rollback()
                return

            # Inserting into DB
            start = time.time()
            sched_table.insert(dict(id=self.curr_sched_id, name=schedule_name, start_datetime=start_datetime,
                                    end_datetime=end_datetime, stored_datetime=curr_datetime,
                                    days_of_week=days_of_week, marquee_text=_read_unicode(marquee),
                                    priority=priority, is_active=0))
            self.logger.info("Schedule [%s] metadata successfully stored in the database (%.2f ms)" %
                             (self.curr_sched_id, (time.time() - start) * 1000))

            skipped_files = []
            for fn in js['content_files']:
                start = time.time()
                # Checking if JSON's structure for file metadata is correct
                fnm = _write_unicode(fn['filename'])
                try:
                    f_type = _write_unicode(fn['type'])
                    f_interval = fn['interval']
                    f_location = _write_unicode(fn['path'])
                    f_playorder = fn['play_order']
                except KeyError, err:
                    self.logger.warning("Error parsing data from JSON content-file [%s]. "
                                        "Skipping this file. Details: %s" % (fnm, err))
                    skipped_files.append(fnm)
                    continue  # Try to store next file if any

                curr_file_id = file_table.insert(dict(name=fnm, type=f_type, interval=f_interval,
                                                      location=f_location, downloaded_yet=0))

                # Inserting files id into mapping table
                file_sched.insert(dict(schedule_id=self.curr_sched_id, file_id=curr_file_id, play_order=f_playorder))

                self.logger.info("File [%s]'s metadata successfully stored in the database (%.2f ms)" %
                                 (fnm, (time.time() - start) * 1000))

            self.db.commit()
            if len(skipped_files) == 0:
                self.logger.debug("Finished saving JSON file into database. Successfully saved ALL files' entries.")
            else:
                self.logger.warning("Finished saving JSON file into database. Saved only SOME files' entries. "
                                    "List of skipped files: %s" % skipped_files)
        # If we need to delete a schedule
        else:
            self.logger.info("Requested to delete schedule [%s] from database..." % self.curr_sched_id)
            if sched_table.count(id=self.curr_sched_id) != 0:
                self.db.begin()
                sched_table.delete(id=self.curr_sched_id)
                file_sched.delete(schedule_id=self.curr_sched_id)
                self.delete_orphan_files()
                self.logger.info("Schedule [%s] was successfully removed from database." % self.curr_sched_id)
                self.db.commit()
            else:
                self.logger.info("No such schedule [%s]." % self.curr_sched_id)

        # If currently playing/to play schedule is the one modified or deleted then player had been stopped before
        # effecting any operation on sched_to_play. Re-start player
        
	# TODO: Since we set the flag when it is ready it will resume
	#if sched_to_play == self.curr_sched_id:
        #    _resume_player()


    ###################################################################################################################
    # Queries helpers
    def delete_orphan_files(self):
        """
        Deletes all the files that have no assigned schedule. In other words it removes all the file entries at
        the ``File`` relation which are not mapped in ``file_sched`` intermediary relation.

        Warning:
            It logs an error in case of rasing any exception.
        """
        try:
            self.logger.info("Trying to delete orphans from database (if any).")
            self.db.query('DELETE FROM file WHERE NOT (EXISTS (SELECT * '
                          'FROM file_sched, schedule '
                          'WHERE file.id = file_sched.file_id AND schedule.id = file_sched.schedule_id))')
        except Exception, err:
            self.logger.error("Error deleted orphan files. Details: %s" % err)

    def delete_orphans_schedules(self):
        """
        Deletes all schedules that have not a single file assigned to them.

        Warning:
            It logs an error in case of rasing any exception.
        """
        try:
            self.logger.info("Trying to delete schedule orphans from database (if any)")
            self.db.query('DELETE FROM schedule WHERE NOT (EXISTS (SELECT * '
                          'FROM file_sched WHERE schedule_id = id))')
        except Exception, err:
            self.logger.error("Error deleted orphan schedules. Details: %s" % err)

    ###################################################################################################################
    # Schedule(s) methods
    def is_sched_active(self, schedule_id):
        """
        Checks if a given schedule (id) is active or no. Meaning that an schedule is ready to play and all its files
        have been successfully downloaded.

        Args:
            schedule_id (int): schedule id

        Returns:
            is_active (bool): ``True`` if the given ``schedule_id`` is active, ``False`` otherwise.
        """
        is_active = False
        try:
            result = self.db['schedule'].find_one(id=schedule_id)
            if result['is_active'] == 0:
                is_active = False
            else:
                is_active = True
        except TypeError:
            self.logger.error("Error retrieving is_active for schedule [%s]. Setting to False."
                              "No such schedule exists in the database." % schedule_id)
        except Exception, err:
            self.logger.error("Error retrieving is_active for file [%s]. Setting to False. "
                              "Details: %s" % (schedule_id, err))

        return is_active

    def flip_sched_is_active(self, id):
        """
        Flips the ``is_active`` column for a given schedule id (if it is 0 it flips it to 1 and vice versa)

        Args:
            id (int): given schedule id

        Returns:
            new_value (bool): New value of ``is_active``: ``True`` if old value of ``is_active`` was 0. ``False``
            otherwise.

        Warning:
            It logs an error if there is no such schedule id, or if there is an exception raised while trying to change
            the field ``is_active`` for the given schedule id.
        """
        curr_value = self.is_sched_active(id)

        if curr_value == 0:
            new_value = 1
        else:
            new_value = 0
        try:
            self.db['schedule'].update(dict(id=id, is_active=new_value), ['id'])
        except TypeError, err:
            self.logger.error("Error setting is_active for sched [%s]."
                              "No such sched exists in the database. %s" % (id, err.message))
            new_value = curr_value
        except Exception, err:
            self.logger.error("Error setting is_active for sched [%s]. Details: %s" % (id, err))
            new_value = curr_value

        return bool(new_value)

    def get_sorted_list_schedules(self):
        """
        Returns:
            schedule_list (list of ints): List of schedules ids **organized** by their **priorities**.

        Warning:
            Logs a warning if it fails to execute the query.
        """
        schedules_list = list()
        try:
            result = self.db.query('SELECT * FROM schedule ORDER BY priority')
            for row in result:
                schedules_list.append(row['id'])
        except TypeError:
            self.logger.warning("Failed to get list of schedule(s) from database.")

        return schedules_list

    def get_list_schedules(self):
        """
        Returns:
            schedules_list (list of ints): List of schedules ids (without any particular order)

        Warning:
            Logs a warning if it fails to execute the query.
        """
        schedules_list = list()
        try:
            result = self.db['schedule'].all()
            for row in result:
                schedules_list.append(row['id'])
        except Exception, err:
            self.logger.warning("Failed to get list of schedule(s) from database. "
                                "Returning empty list. Details: %s" % err)

        return schedules_list

    def get_sched_to_play(self, curr_day, curr_datetime):
        """
        Args:
            curr_day (str): Current day of the week in a 3 character format.
            curr_datetime (str): Current date and time in a formatted form:``YYYY-MM-DD HH:MM:SS``.

        Returns:
            sched_id (int): The id of the schedule that needs to be played based on the current day of the week and
            current date and time.

        Warning:
            It will only consider schedules that are **active**.
        """
        sched_to_play = None
        try:
            result = self.db.query(
                'SELECT * FROM schedule'
                ' WHERE (date(\"' + curr_datetime + '\")'
                ' BETWEEN date(start_datetime) AND date(end_datetime)) AND (time(\"' + curr_datetime + '\") '
                ' BETWEEN time(start_datetime) AND time(end_datetime)) AND is_active = 1 ORDER BY priority DESC, datetime(stored_datetime) DESC LIMIT 1')
            for row in result:
                if curr_day in row['days_of_week'].split(','):
                    sched_to_play = row['id']
        except Exception, err:
            self.logger.error("Failed to get schedule to play for [%s] and [%s]"
                              "from database. Returning empty string. Details: %s" % (curr_datetime, curr_day, err))
        return sched_to_play

    def get_list_files_and_locations(self, sched_id=None):
        """
        Args:
            sched_id (Optional[int]): schedule id. Defaults to None; in which case we use the **latest added schedule** \
            's id using ``get_latest_sched_id()`` method.

        Returns:
            file_location_list (list of dict): list of locations and file names for all the files for the given schedule
        """
        if not sched_id:
            sched_id = self.get_latest_sched_id()

        file_location_list = list()

        try:
            result = self.db.query("SELECT file.location, file.name FROM file"
                                   " INNER JOIN file_sched ON file.id = file_sched.file_id"
                                   " AND file_sched.schedule_id = " + str(sched_id) +
                                   " ORDER BY file_sched.play_order ASC")
            for row in result:
                file_location_list.append(dict(location=row['location'].encode('utf-8'), name=row['name']))
        except Exception, err:
            self.logger.warning("Failed to get file names and location list for [%s] from database. "
                                "Returning empty list. Details: %s" % (sched_id, err))

        return file_location_list

    def get_list_files(self, sched_id=None):
        """
        Args:
            sched_id (Optional[int]): schedule id. Defaults to None; in which case we use the **latest added schedule** \
            's id using ``get_latest_sched_id()`` method.

        Returns:
            list_files (list of str): List of file names given an schedule id, or an empty list in case of an error.

        Warning:
            It logs a warning if there is an error querying the list of files, and returns an empty list.
        """

        if not sched_id:
            sched_id = self.get_latest_sched_id()

        list_files = list()

        try:
            result = self.db.query(
                'SELECT file.name FROM file INNER JOIN file_sched ON (file.id = file_sched.file_id AND '
                'file_sched.schedule_id = ' + str(sched_id) + ')ORDER BY file_sched.play_order ASC')
            for row in result:
                list_files.append(row['name'].encode('utf-8'))
        except Exception, err:
            self.logger.warning("Failed to get list of file for [%s] from database. Returning empty list."
                                "Details: %s" % (sched_id, err))

        return list_files

    def get_list_files_locations(self, sched_id=None):
        """
        Returns:
            location_list (list of str): List of locations for all the files for the given schedule id.

        Args:
            sched_id (Optional[int]): schedule id. Defaults to None; in which case we use the **latest added schedule** \
            's id using ``get_latest_sched_id()`` method.

        Warning:
            It logs a warning if there is an error querying the list of files, and returns an empty list.
        """

        if not sched_id:
            sched_id = self.get_latest_sched_id()

        locations_list = list()

        try:
            result = self.db.query \
                ("SELECT file.location FROM file INNER JOIN file_sched ON file.id = file_sched.file_id AND "
                 "file_sched.schedule_id = " + str(sched_id) + " ORDER BY file_sched.play_order ASC")
            for row in result:
                locations_list.append(row['location'].encode('utf-8'))
        except Exception, err:
            self.logger.warning("Failed to get location list for [%s] from database. Returning empty list."
                                "Details: %s" % (sched_id, err))

        return locations_list

    def get_latest_sched_id(self):
        """
        Returns:
            latest_schedule (int): Latest added schedule id. It does this by calling ``get_schedule_list()`` method,
            and returning the last element of its list.

        Warning:
            It logs a warning if there is no even one schedule in the database.
        """
        list_schedules = self.get_list_schedules()

        if len(list_schedules) == 0:
            self.logger.warning("No schedules on the database, returning None.")
            latest_schedule = None
        else:
            latest_schedule = list_schedules[-1]

        return latest_schedule

    def get_days_of_week(self, sched_id=None):
        """
        Returns:
            days_of_week (list of str): List of days of the week to play the given schedule (formatted as a three
            character days)

        Args:
            sched_id (Optional[int]): schedule id. Defaults to None; in which case we use the **latest added \
            schedule** 's id using ``get_latest_sched_id()`` method.

        Warning:
            It logs a warning if there is an error querying the list of days, and returns an empty list.
        """

        if not sched_id:
            sched_id = self.get_latest_sched_id()

        days_of_week = []
        try:
            result = self.db['schedule'].find_one(id=sched_id)
            days_of_week = result['days_of_week'].encode('utf-8').split(',')
        except TypeError:
            self.logger.error("Failed to get start datetime for [%s]. "
                              "Schedule does not exists on the database." % sched_id)
        except Exception, err:
            self.logger.error("Failed to get start datetime for [%s] schedule from database. Details: %s" %
                              (sched_id, err))

        return days_of_week

    def get_start_datetime(self, sched_id=None):
        """
        Returns:
            start_datetime (str): Start date and time in a formatted string.

        Args:
            sched_id (Optional[int]): schedule id. Defaults to None; in which case we use the **latest added \
            schedule** 's id using ``get_latest_sched_id()`` method.

        Warning:
            It logs a warning if there is an error querying the date and time, and returns an empty string.
        """

        if not sched_id:
            sched_id = self.get_latest_sched_id()

        start_datetime = DEF_START_AND_END_DT
        try:
            result = self.db['schedule'].find_one(id=sched_id)
            start_datetime = result['start_datetime'].encode('utf-8')
        except TypeError:
            self.logger.error("Failed to get start datetime for [%s]. "
                              "Schedule does not exists on the database." % sched_id)
        except Exception, err:
            self.logger.warning("Failed to get start datetime for [%s] schedule from database. Details: %s" %
                                (sched_id, err))

        return start_datetime

    def get_end_datetime(self, sched_id=None):
        """
        Returns:
            end_datetime (str): End date and time in a formatted string.

        Args:
            sched_id (Optional[int]): schedule id. Defaults to None; in which case we use the **latest added \
            schedule** 's id using ``get_latest_sched_id()`` method.

        Warning:
            It logs a warning if there is an error querying the date and time, and returns an empty string.
        """
        if not sched_id:
            sched_id = self.get_latest_sched_id()

        end_datetime = DEF_START_AND_END_DT
        try:
            result = self.db['schedule'].find_one(id=sched_id)
            end_datetime = result['end_datetime']
        except TypeError:
            self.logger.error("Failed to get end datetime for [%s]. "
                              "Schedule does not exists on the database." % sched_id)
        except Exception, err:
            self.logger.warning("Failed to get end time for [%s] schedule from database. Details: %s" %
                                (sched_id, err))

        return end_datetime

    def get_sched_marquee(self, sched_id=None):
        """
        Returns:
            marquee_text (str): Schedule's marquee text (if any)

        Args:
            sched_id (Optional[int]): schedule id. Defaults to None; in which case we use the **latest added \
            schedule** 's id using ``get_latest_sched_id()`` method.

        Warning:
            It logs a warning if there is an error querying the marquee text, and returns an empty string.
        """

        if not sched_id:
            return ""

        marquee_text = ""
        try:
            result = self.db['schedule'].find_one(id=sched_id)
            marquee_text = _read_unicode(result['marquee_text'])
        except TypeError:
            self.logger.warning(
                "Failed to get marquee text for [%s] schedule from database. Leaving marquee text empty" % sched_id)

        return marquee_text

    def set_active_schedules(self):
        """
        Checks the downloaded files, if all the downloaded files for a schedule are downloaded sets its flag
        ``is_active`` to ``True``, ``False`` otherwise.

        Warning:
            It logs an error if there is an problem getting a list of schedules, and their files.
        """
        try:
            all_sched_ids = self.get_list_schedules()

            for index in all_sched_ids:
                if not (self.is_sched_active(index)):
                    all_files = self.get_list_files(index)
                    flag = True
                    for f in all_files:
                        if not (self.is_file_downloaded_yet(f)):
                            flag = False
                    if flag:
                        self.flip_sched_is_active(index)
        except Exception, err:
            self.logger.error("Failed to update is_active for schedules in the database. "
                              "This might cause schedules not showing. Details: %s", err)

    ####################################################################################################################
    # File(s) methods

    def get_file_interval(self, file_name):
        """
        Args:
            file_name (str): File name.

        Returns:
            interval (int): Given file's interval time in seconds

        Warning:
            It logs a warning if it fails to get the interval time for the given file, and sets the interval to the
            ``DEF_INTERVAL``
        """
        try:
            result = self.db['file'].find_one(name=file_name)
            interval = result['interval']
        except TypeError:
            self.logger.warning("Failed to get interval time for file [%s] from db. "
                                "Setting interval to %d." % (file_name, DEF_INTERVAL))
            interval = DEF_INTERVAL

        return interval

    def get_list_all_files(self):
        """
        Returns:
            list_files (list of str): List of file names present in the ``File`` relation.

        Warning:
            - The output of this method might include files which are not being used by **any** schedule. These we \
            refers as *orphan files*. They can be deleted via the ``delete_orphan_files()`` method.

            - This ensures that if there is a new incoming schedule which uses an already downloaded but unused file \
            (*orphan file*) we would not need to download it again.

                - *orphan files* are only deleted via the ``clean_memory.py`` module.
        """
        list_files = list()
        try:
            result = self.db['file'].all()
            for row in result:
                list_files.append(row['name'])
        except TypeError, err:
            self.logger.exception("Failed to get list of current files. Returning empty string. Details: %s" % err)

        return list_files

    def get_file_type(self, file_name):
        """
        Retrieves a file's type.

        Args:
            file_name (str): File name.

        Returns:
            f_type (str): Returns the file type of the given file name (``IMAGE``, ``VIDEO``, etc).

        Warning:
            It logs an error in the case that there is no such file, or if there is an error retrieving the file type,
            and thus returns an empty string.
        """
        f_type = ""
        try:
            result = self.db['file'].find_one(name=file_name)
            f_type = result['type'].encode('utf-8')
        except TypeError:
            self.logger.error("Error retrieving type for file [%s]. No such file exists in the database." % file_name)
        except Exception, err:
            self.logger.error("Error retrieving type for file [%s]. Details: %s." % (file_name, err))

        return f_type

    def is_file_downloaded_yet(self, file_name):
        """
        Checks whether a file has been downloaded or not.

        Args:
            file_name (str): File name.

        Returns:
            downloaded_yet (bool): ``True`` if the given file name has been already downloaded. Indicated by its \
            ``downloaded_yet`` flag. ``False`` otherwise.

        Warning:
            It logs an error in the case that there is no such file, or if there is an error retrieving its flag, and
            thus returns ``False``.
        """
        downloaded_yet = False
        try:
            result = self.db['file'].find_one(name=file_name)
            if result['downloaded_yet'] == 0:
                downloaded_yet = False
            else:
                downloaded_yet = True
        except TypeError:
            self.logger.error("Error retrieving downloaded_yet for file [%s]. Setting to False."
                              "No such file exists in the database." % file_name)
        except Exception, err:
            self.logger.error("Error retrieving downloaded_yet for file [%s]. Setting to False. "
                              "Details: %s" % (file_name, err))

        return downloaded_yet

    def is_file_downloading(self, file_name):
        """
        Checks whether a file is currently being downloading or not.

        Args:
            file_name (str): File name.

        Returns:
            downloading (bool): ``True`` if the given file name is being downloading. Indicated by its \
            ``downloaded_yet`` flag. ``False`` otherwise.

        Warning:
            It logs an error in the case that there is no such file, or if there is an error retrieving its flag, and
            thus returns ``False``.
        """
        downloading = False
        try:
            result = self.db['file'].find_one(name=file_name)
            if result['downloaded_yet'] == FLAG_DOWNLOADING:
                downloading = True
            else:
                downloading = False
        except TypeError:
            self.logger.error("Error retrieving download_yet for file [%s]. Setting to False."
                              "No such file exists in the database." % file_name)
        except Exception, err:
            self.logger.error("Error retrieving download_yet for file [%s]. Setting to False. "
                              "Details: %s" % (file_name, err))

        return downloading

    def set_downloaded_yet(self, file_name, new_value):
        """
        Sets the ``downloaded_yet`` flag.

        Args:
            file_name (str): File name.
            new_value (int): New ``downloaded_yet`` value.

        Warning:
            It logs an error if the ``new_value`` is not a valid one (``0: Not downloaded``, ``1: downloaded``, \
            ``2: downloading``), or if there is an error setting the flag.
        """
        # If new value is not allowed return and log error
        if new_value not in [FLAG_DOWNLOADED, FLAG_DOWNLOADING, FLAG_NOT_DOWNLOADED]:
            self.logger.error("downloaded_yet can only take 3 values: 0 NOT downloaded, 1 downloaded, 2 downloading")
            return

        try:
            self.db['file'].update(dict(name=file_name, downloaded_yet=new_value), ['name'])
        except TypeError, err:
            self.logger.error("Error setting download_yet for file [%s]."
                              "No such file exists in the database. %s" % (file_name, err.message))
        except Exception, err:
            self.logger.error("Error setting download_yet for file [%s]. Details: %s" % (file_name, err))

    def _get_files_given_downloaded_yet_value(self, value):
        """
        Args:
            value (int): value to check against a ``downloaded_yet``.

        Returns:
            list_of_files (list of dict): List of dictionaries mapping file name, and file location for all the files
            that its ``downloaded_yet`` flag maps the passed argument ``value``.

        Warning:
            It logs an error if there is problem running the query, and returns **empty list**.
        """
        list_of_files = []
        try:
            result = self.db.query('SELECT file.name, file.location FROM file WHERE downloaded_yet = ' + str(value))
            for row in result:
                list_of_files.append(dict(name=row['name'], location=row['location']))
        except Exception, err:
            self.logger.error("Error retrieving files based on downloaded_yet column from database. Details: %s" % err)

        return list_of_files

    def get_files_to_download(self):
        """
        Returns:
            list_of_files (list of dict): list of dictionaries mapping file names and locations, which need to be
            downloaded.

        Warning:
            It makes internal calls to ``_get_files_given_downloaded_yet_value()`` method.
        """
        list_of_files = self._get_files_given_downloaded_yet_value(FLAG_NOT_DOWNLOADED)
        list_of_files += self._get_files_given_downloaded_yet_value(FLAG_DOWNLOADING)
        return list_of_files

    def get_downloaded_files(self):
        """
        Returns:
            list_of_files (list of dict): list of dictionaries mapping file names and locations, which has been already
            downloaded.

        Warning:
            It makes an internal call to ``_get_files_given_downloaded_yet_value()`` method.
        """
        return self._get_files_given_downloaded_yet_value(FLAG_DOWNLOADED)

    def get_downloading_files(self):
        """
        Returns:
            list_of_files (list of dict): list of dictionaries mapping file names and locations, which are being
            downloaded.

        Warning:
            It makes an internal call to ``_get_files_given_downloaded_yet_value()`` method.
        """
        return self._get_files_given_downloaded_yet_value(FLAG_DOWNLOADING)

    ####################################################################################################################
    # Marquee table methods

    def get_marquee_metadata(self):
        """
        Returns:
            list_marquees (list of dict): list of marquees' metadata (text, start and end date and time).

        Warning:
            It logs an error if there is a problem retrieving an urgent marquee. Thus, returning an empty list.
        """
        list_marquees = []
        # TODO: Use freeze dataset to get json
        try:
            result = self.db.query('SELECT * FROM marquee ORDER BY id DESC')
            for row in result:
                metadata = json.dumps(dict(text=row['text'], start_datetime=row['start_datetime'],
                                           end_datetime=row['end_datetime']), ensure_ascii=False)
                list_marquees.append(metadata)
        except Exception, err:
            self.logger.error("Error getting metadata from urgent marquee. Returning empty list. Details %s" % err)

        return list_marquees

    def get_urgent_marquee_to_play(self, curr_datetime):
        """
        Args:
            curr_datetime (str): String containing date and time information.

        Returns:
            marquee_text (str): Queries the database for an urgent marquee to display given that the current time is
            between start datetime and end datetime. If there is more than one urgent marquee to play then it randomly
            chooses 1 by ``LIMIT`` ing to 1 the query. Also, if there is no a marquee to play then it returns an
            **empty string**.

        Warning:
            It logs an error if there is a problem retrieving an urgent marquee text. Thus, returning an empty string.
        """
        marquee_text = ""
        try:
            result = self.db.query(
                'SELECT * FROM marquee '
                ' WHERE (date(\"' + curr_datetime + '\") BETWEEN date(start_datetime) AND date(end_datetime)) '
                ' AND (time(\"' + curr_datetime + '\") BETWEEN time(start_datetime) AND time(end_datetime)) LIMIT 1 ')
            for row in result:
                marquee_text = _read_unicode(row['text'])
        except Exception, err:
            self.logger.error("Error getting get_urgent_marquee_to_play from urgent marquee. "
                              "Returning empty list. Details %s" % err.args)
        return marquee_text

    ####################################################################################################################
    # Deletions methods

    def delete_expired_marquees(self):
        """
        Deletes all expired urgent marquess by looking at their end dates and comparing them to the current date.

        Warning:
            It logs an error, and rolls back the database if there is any error while deleting an urgent marquee entry.
        """
        self.logger.info("Trying to delete all expired urgent marquees.")
        list_deletd_marquee = []

        self.db.begin()
        try:
            curr_date = time.strftime("%Y-%m-%d")
            result = self.db.query('SELECT id FROM marquee WHERE date(end_datetime) < date(\"' + curr_date + '\")')
            self.db.query('DELETE FROM marquee WHERE date(end_datetime) < date(\"' + curr_date + '\")')

            for row in result:
                list_deletd_marquee.append(row['id'])

            self.db.commit()
            if len(list_deletd_marquee) != 0:
                self.logger.info("Deleted all marquees that have expired (current date: %s). "
                                 "List of deleted marquees' ID: %s" % (curr_date, list_deletd_marquee))
            else:
                self.logger.info("No urgent marquees to delete. Either there is no urgent marquee(s) or "
                                 "all haven't expired yet.")
            return list_deletd_marquee
        except Exception, err:
            self.db.rollback()
            self.logger.error("Error deleting all expired marquees. Details: %s" % err)

    def delete_file(self, file_name):
        """
        Deletes a file, given a file name from the database.

        Args:
            file_name (str): File name to delete.

        Warning:
            It logs an error and rolls back thed database if there is any error while trying to delete the file.
        """
        self.db.begin()
        try:
            self.db.query(
                'DELETE FROM file_sched WHERE file_id = (SELECT id FROM file WHERE name = \"' + file_name + '\")')
            self.delete_orphan_files()
            self.db.commit()
        except Exception, err:
            self.db.rollback()
            self.logger.error("Error deleting file [%s] from database. Details: %s" % err)

    def delete_expired_schedules(self):
        """
        Deletes all schedule that have expired from the database. An schedule expiration is defined if ``end_date`` is
        less than the current date.

        Returns:
            list_deleted_scheds (list of str): List of all deleted schedules that have expired. If no schedule has been
            deleted either due to an error or lack of schedules to be deleted, an empty list will be returned.

        Example:
            >>> import time
            >>> time.strftime("2015.01.01") < time.strftime("2015-01-02")
            True

        Warning:
            It logs an error and rolls back the database if there is any error while trying to delete an schedule.
        """
        self.logger.debug("Trying to delete all expired schedules.")
        list_deleted_scheds = []

        self.db.begin()  # Begin database transaction
        try:
            #  Get current date YYY-MM-DD and query all expired schedules. Then delete them
            curr_date = time.strftime("%Y-%m-%d")
            result = self.db.query('SELECT id FROM schedule WHERE date(end_datetime) < date(\"' + curr_date + '\")')
            self.db.query('DELETE FROM schedule WHERE date(end_datetime) < date(\"' + curr_date + '\")')

            for row in result:  # Delete all entries of deleted schedules from file_sched mapping table
                self.db.query('DELETE FROM file_sched WHERE schedule_id = ' + str(row['id']))
                list_deleted_scheds.append(row['id'])

            self.delete_orphan_files()  # Delete orphan files
            self.db.commit()  # Commit queries to database
            if len(list_deleted_scheds) != 0:
                self.logger.info("Deleted all schedules that have expired (current date: %s). "
                                 "List of deleted schedules' IDs: %s" % (curr_date, list_deleted_scheds))
            else:
                self.logger.info("No schedules to delete. Either  there is no schedules or all haven't expired yet.")

        except Exception, err:
            self.db.rollback()
            self.logger.error("Error deleting all expired schedules. Details: %s" % err)

        return list_deleted_scheds


########################################################################################################################
# Utility functions to control player process

def _stop_player():
    """
    Stops the ``player`` process, via ``supervisorctl``

    Warning:
        This is internally in the case we want to update the currently playing schedule.
    """
    os.system("sudo supervisorctl stop coe_client_player")


def _resume_player():
    """
    Starts the ``player`` process, via ``supervisorctl``

    Warning:
        This is internally in the case we want to update the currently playing schedule.
    """
    os.system("sudo supervisorctl start coe_client_player")


########################################################################################################################
# Utility functions to write and read unicode

def _write_unicode(text, charset='utf-8'):
    """
    Utility function that encodes a given text and some charset (by default ``UTF-8``)

    Args:
        text (str): String of text.
        charset (Optional[str]): Charset type to encode the ``text``. Defaults to ``utf-8``.

    Returns:
        . (str): Encoded text in given charset.

    Example:
        >>> S = u'Test String'
        >>> codecs.encode(S, 'utf-8')
        "Test String"
    """
    return text.encode(charset)


def _read_unicode(text, charset='utf-8'):
    """
    Utility function that reads a text in a given charset (by default ``utf-8``)

    Args:
        text (str): String of text.
        charset (Optional[str]): Charset type to read from. Defaults to ``utf-8``

    Returns:
        text (str): Text redable in given charset.
    """
    if isinstance(text, basestring):
        if not isinstance(text, unicode):
            text = unicode(text, charset)
    return text
