>> **Important**: Moving documentation [here](http://snuarduinolabs.github.io/coe_client)

# `coe_client`
**What is it ?** Client side of a news & multimedia system implemented for the college of Engineering at Seoul National University.

# Index

1. [Introduction](#introduction)
2. [Overview](#overview)
3. [Set up](#set-up)
4. [Architecture](#architecture)
    1. [Database](#database)
    2. [Components Description](#components-description)
    3. [Helper Modules/Scripts](#helper-modules/scripts)
5. [Additional Information](misc/README.md)

# Introduction
Multimedia broadcasting systems are an effective method to convey information to a wide audience spread over a large region. The College of Engineering (CoE) at Seoul National University utilizes the same method to promptly deliver news and recent announcements, PR videos, and other useful information to thousands of students spread over tens of separate buildings. The current system is based on Windows operating system and has some drawbacks.

Arduino Labs is a student Club founded at Seoul National University in December of 2012 where both Korean and international students meet weekly to work on innovative and cross-disciplinary projects based on Arduino, Raspberry Pi etc. Here at Arduino Labs, we strongly believe in the significance of open source and its potential to solve difficult problems thanks to the mutual collaboration in the open-source communities. From the knowledge and expertise gained from the past projects, Arduino Lab has been assigned the task to reimplement the current broadcasting system, overcoming its current drawbacks and adding some reasonable improvements to the system.

# Overview:

The `coe_client` system has three main goals:
1. Store and manage playlist schedules.
2. Download scheduled files (videos, and images) from the server.
3. Reproduce the playlists on a TV/Display connected to a Raspberry Pi.

Due to these three independent goals, we have divided `coe_client` in three main Python programs (`downloader.py`, `player.py`, and `webserver,py`). The interprocess communication between these three programs is handled by a database (`sqlite3`). Note that the database access is abstracted by the `db_manager.py`.

The following image shows the interaction between the server and client, using JSON files as medium to communicate information. Note that, the server communicates playlists' metadata via a JSON file that is `POST` (by the cliet) at the `/schedule` URL. Similarly, urgent marquee metadata is pushed to the client by the server at `/marquee` URL. Moreover, the server can pull log-data from the client by making a GET request at the `/status` URL.

<div style="text-align:center"><img src="http://i.imgur.com/yHJAonK.png" width="720"></div>

All the three Python programs, and other separate scripts, are managed using the `supervisor` daemon; "a client/server system that allows its users to monitor and control a number of processes on UNIX-like operating systems" (see more [here](http://supervisord.org/)). Therefore, the `supervisord` has full control (`stop`/`restart`/`start`), and it is responsible for starting the programs when the Raspberry Pi boots.

<div style="text-align:center"><img align="center" src="http://i.imgur.com/Gxnn483.png" width="420"></div>

For more detailed information regarding the three main Python programs, please read the [component description](#components-description) section.

> As the time of this writing (Nov 2015), `supervisord` controls `downloader.py`, `player.py`, `webserver.py` and `clean_memory.py`.


# Set up
Currently, there are two ways to set up the client on a Raspberry Pi, a recommend set of steps that uses a pre-installed Raspian OS image, and a Error-prone set of steps that uses shell scripts to install and configure the necessary things.

> During this document we assume that `$COE_HOME` points to `home/coe_client` as described in the set-up scripts.

## Recommended way
1. Flash a `coe_client` pre-installed Raspian OS image into a micro SD card. For this, download the image [here](https://drive.google.com/file/d/0By3yluHw9TsuNHQ4NEJNRU9LOGM/view?usp=sharing), and copy it to an micro SD card.

    - If you are using some flavor of Unix use `dd` to copy it into the SD card.

    Example:`dd if=/path/to/img of=/path/to/umounted/dev/sd`

    - If you are using Windows follow the step provided [here](http://elinux.org/RPi_Easy_SD_Card_Setup).

2. Run `sudo raspi-config` and expand file system.

3. Follow this [extra configuration steps.](#extra-configuration)

> For detailed information about how to use `dd`, or how `supervisor` works go [here](misc/README.md#copying-a-img-file-to-sd-card).

## Error-prone way

1. Set up a Raspberry Pi following [this](misc/README.md#set-up-a-rbpi) instructions.

2. Download and run the `setup.sh` script provided inside the `misc/` directory.

    ```bash
    curl https://raw.githubusercontent.com/SNUArduinoLabs/coe_client/master/misc/setup.sh > setup.sh
    chmod +x setup.sh
    ./setup.sh
    ```
3. Follow this [extra configuration steps.](#extra-configuration)

> For detailed information about what does the `setup.sh` script does go to [misc](misc/README.md).

## Extra configuration

> We recommend that the first time a Raspberry Pi is set up with an specific TV (or display) connect the TV first to the Raspberry Pi, and then proceed to turn the TV on. If the prompted image does not seem to appear correctly on the TV/Display (larger/smaller than the TV area) then run `sudo raspi-config` and dissable/enable **overscan**.

1. Edit the configuration `$COE_HOME/misc/coe_client.conf` file with the server's information.
    - It should contain the server's `hostname` (URL o IP), `username` and `port` **in that order**.
    - Example:

    ```
    [server]
    hostname=118.91.109.214
    username=cacyss0807
    port=2000
    ```
> Only modify the necessary values, changes to other parts of the configuration file might cause errors.

2. Create a pair of (empty pass-phrase) private and public keys and copy them to the remote server.

 ```
 $ ssh-keygen -t rsa -b 2049
 $ ssh-copy-id <user>@<server url/ip>
 ```
 - For a detailed example go [here](misc/README.md#creating-pair-of-private-and-public-keys)

3. Copy the `supervisor_player.conf` file to the supervisor `conf.d` directory. This can be easily done by running the **`copy_player_conf.sh` script**, placed in the `misc` directory.

 ```
 $ cd $COE_HOME/misc
 $ sh copy_player_conf.sh
 ```

 Or, it can be done manually, by looking at how `supervisord` works at [how to set up `supervisord`](misc/README.md#how-to-set-up-supervisord).

 > It is very important to do this step at the **end** of the configuration process; or at least after having `ssh` access to the Raspberry Pi, because if the `player` is activated from boot there might not be way to see a prompt.

# Architecture
We will now describe in detail each one of the main modules/programs introduces in the [overview](#overview). However, we will first start by describing the database schema, and the database manager that abstract the communication with the three main programs.

## Database
The database schema is the following:

<div style="text-align:center"><img src="http://i.imgur.com/GKpGv46.png" width="420"></div>


- There are two main tables, which are called `schedule` and `file`, with a many-to-many mapping.
- The third table `marquee` stores **urgent** marquees that override an schedule's own marquee.
- The database file name is: `coe_client.db` and is stored at `$COE_HOME/src/db/` directory.
- It can be created using the Python program: `create_db.py` (located on `$COE_HOME/src`).

### `schedule` table
| Column name       | Description     |
| :------------     | :------------- |
| `id`              | Schedule's id to uniquely identify a schedule (**NOT** autoincremented) |
|`name`             | Schedule's name (not heavily used)|
|`start_datetime`   | Schedule's start date and time (format: `YYYY-MM-DD HH:MM:SS`)|
|`end_datetime`     | Schedule's end date and time (format: `YYYY-MM-DD HH:MM:SS`)|
|`days_of_week`     | List of days on which the schedule is active (example: `MON,FRI`)|
|`marquee_text`     | Schedule's marquee text.|
|`priority`         | Schedule's priority (the higher the number the lower the priority)|
|`is_active`        | Flag used to specify that a schedule's files are ready to play.|

#### Notes

- The Schedule's `id` is **NOT** auto incremented because we use the same `id` that the server side of the system uses. We opted for this approach so that if there is an error or something goes wrong we could pin point exactly which schedule generated the error/missbehaviour.
- From the `end_datetime` we can extract the expiration date of a given schedule. Hence, it is possible to remove all *expired* schedules (see [`clean_memory.py`](#clean_memory)).
- `days_of_week` is a string containing the strings of length 3 separated by a comma which represent a day of the week (we use days names prefix of size 3).
- `marquee_text` that should be display only along aside the currently active schedule.
- The `priority` is used to choose from a (possible) set of overlapping schedule to play.
- The `is_active` flag is set only when **all** the files have been successfully downloaded.
    - If a file fails to downloaded for more than *x* (5) times it will get deleted from the database. Thus, that file will not longer belong to that schedule. Also, any entries from that file that map to any schedule will get deleted. This, avoids trying to downloaded a file for a long period of time.

### `file` table
Stores meta-data for a file. It is composed of the following columns:

| Column name   | Description |
|:--------------|:------------|
|`id`           | File id to uniquely identify a file (autoincremented). |
|`name`         | File name as sent from the remote host / server side|
|`type`         | File type (either `IMAGE`, `VIDEO`, or `PDF`) |
|`interval`     | Time in seconds to display a file of type `IMAGE`|
|`location`     | Path to file at the remote host.|
|`downloaded_yet`| Flag to identify when a file has been successfully downloaded |

#### Notes
- The file `name` is its checksum (SHA1); with no extension appended.
- As of now every file gets converted to `VIDEO` by the server.
- The `interval` is ignored for now.
- The `downloaded_yet` flag is set/unset from the `downloader`.

### `marquee` table
Stores **urgent** marquee meta-data. It is composed of the following columns.

| Column name       | Description |
| :-------------    | :------------- |
| `id`              | Marquee's id to uniquely identify it (autoincremented) |
| `text`            | Marquee's content (`utf-8`)|
|`start_datetime`   | Marquee's start date and time (format: `YYYY-MM-DD HH:MM:SS`)|
|`end_datetime`     | Marquee's end date and time (format: `YYYY-MM-DD HH:MM:SS`)|

#### Notes

- An **urgent** marquee is used to communicate an important or *urgent* notice. Therefore, there are only start and end date times, and no `days_of_week`. This is because an urgent marquee is supposed to be used for a couple of minutes of hours.
- Automatic deletion of expired marquee is done on the `clean_memory.py` program (see [`clean_memory.py`](#clean_memory)).

----
## Notes when reading Python code:

- an `_` (under score) before a method indicates that it is a private method.
- Default values to parameters are indicated by setting them to a value on the argument declaration.

## Components Description

The `schedule`, `file` and urgent `marquee` meta-data gets transferred from the remote host (server) to the clients via a `JSON` file. This files is parsed by the database. Here is an example file:

1. Schedule `JSON`:
```json
    {
        "action": "add",
        "schedule_name": "sched3",
        "schedule_id": 125,
        "start_datetime": "2015-01-01 06:00:00",
        "end_datetime": "2016-12-31 18:59:59",
        "days_of_week": ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"],
        "marquee_text": "Some banner text",
        "priority": 1,
        "content_files":
        [
            {
                "filename": "1.jpg",
                "type": "IMAGE",
                "path": "/some/location",
                "playback_time": 23,
                "interval": 1,
                "play_order": 2
            }
        ]
    }
```

2. Urgent marquee `JSON`:
```json
{
        "text": "some content for the marquee (한국어 잘 될까요?)",
        "start_datetime": "2015-01-01 05:01:11",
        "end_datetime": "2016-01-01 10:10:10"
}
```

> Some of the `JSON` file content are not used, but still are being sent. Like the `playback_time`.

### Web-server (`webserver.py`)

Simple and lightweight Flask web-server that listen for `GET` request from the main server at `/schedule` and `/marquee` URLs. It also `POST` status and logging files to via the `/status` URL.

Every time the server `POST` something at `/schedule`, the `db_manager.py` checks the `action` object of the `JSON` file, if `action: add` then:

- If the new schedule already exists in the database, it removes its copy and re-saves its meta-data (and its playlist files' meta-data).
- If it does not exits already the **`db_manager` adds it** (with all the corresponding files entries).

On the other-hand, if `action: del` then the downloader proceeds to delete the given schedule from the database. **Note** that the physical files for that schedule don't get deleted because there might be other schedules in the client that use the same exact file. Thus, deleting physical files is a [`clean_memory.py`](#clean_memory)'s job.

#### Description of methods and constants:

|Method                 |Parameters                 |Description|
|:-----                 |:--------:                 |:----------|
|`get_sched`            |N/A                        |Handles a POST request that receives a JSON file corresponding to a schedule.|
|`get_marquee`          |N/A                        |Handles a POST request that receives a JSON file corresponding to an urgent marquee|
|`set_status`           |N/A                        |Handles a GET request that returns as many number of lines as the query parameters indicates from the log files.|
|`_get_data_from_log`   |`fn, lines`                |Opens a log (`fn`) file and gets the `n` last lines from it, and returns them as a list |
|`_last_n_lines`        |`f, lines=1, _buffer=4098` |Tails a file (`f`) and get `n` lines from the end by retrieving a batch of lines indicated by `_buffer`|

---

|Constant           |Value  |Description|
|:-------           |:---:  |:----------|
|`DEFAULT_LINE_NUM` |5      |Default number of lines to retrieve from the log files in case of error reading query parameters.|
|`TIME_OUT`         |5      |Maximum amount of seconds allowed to save the meta-data of a schedule in the database.|

### Downloader (`downloader.py`)

Separate process that handles the download of files from the remote host (server). It also manages the secure connection to the server via `scp`.

- It tries to download all the files whose flag `downloaded_yet` is `False` (`0`). It keeps track of all files that failed to download, and re-tries to download them *x*(`MAX_DOWNLOAD_TRIES`: 5) times more. If a file fails to download more than that *x* number of times its entry gets deleted from the database.

- It sets a schedule's flag `is_active` to `True` (`1`) only when **all** its files have been downloaded.
    - If a schedule's files have failed more than *x* number of times, since the entries for those files that failed will get deleted from the database, eventually *all* files for a given schedule will have been downloaded successfully.

    - If all the files of a schedule fail to download it will have no files entries, therefore `downloader` tries to **remove all schedules with no files**.

#### Description of methods and constants:

|Constant               |Value                      |Description|
|:-------               |:---:                      |:----------|
|`DEF_PORT`             |`"22"`                     |Default port to connect to incase of error reading the configuration file (`coe_client.conf`).|
|`DEF_HOSTNAME`         |`"192.168.0.35"`           |Default IP/URL to connect to incase of error reading the configuration file (`coe_client.conf`).|
|`DEF_USERNAME`         |`"cacyss0807"`             |Default username to connect to via SSH and SCP incase of error reading the configuration file (`coe_client.conf`)
|`CONF_FILE`            |`"../misc/coe_client.conf"`|Relative path to the configuration file.|
|`DELTA_RE_CHECK_CONF`  |`10`                       |Time delta in seconds. Meaning that downloader re-reads `coe_client.conf` if it has been modified within this amount of time|
|`RSA_KEY_PATH`         |`"~/.ssh/id_rsa"`        |**Absolute** path to the RSA key file|
|`DEF_SSH_TIMEOUT`      |`10`                       |Time out in seconds to connect via SSH|
|`DOWNLOAD_PATH`        |`"~/Downloads"`            |**Absolute** path to directory where files get downloaded|
|`WAIT_TIME_CONN_ERR`   |`30`                       |Time out in second in-case of connection error (SSH/SCP)|
|`WAIT_RECHECK_DB`      |`30`                       |Time in seconds that program sleeps to re-check the database for new files to download.|
|`MAX_DOWNLOAD_TRIES`   |`5`                        |Maximun number of intents to download a file.|
|`NOT_DOWNLOADED`       |`0`                        |Mask to define not downloaded files.|
|`DOWNLOADED`           |`1`                        |Mask to define (completely) downloaded files.|
|`DOWNLOADING`          |`2`                        |Mask to define downloading files.|

---

##### Class: `Downloader`

|Method                 |Parameters     |Description|
|:-----                 |:--------:     |:----------|
|`download_files`       |N/A            |Checks the database and gets a list of all the files that have NOT been downloaded yet. Try to download those. If any of those fail keep track how many times they failed, keep trying to download this set of failed-to-download files until reaching `MAX_DOWNLOAD_TRIES` times, by then just removes this file's entry from the database.|
|`_download_one_file`   |`scp, fn, loc` |Downloads a given file (`fn`) from a given remote location (`loc`) to the `DOWNLOAD_PATH`; using a `SCPClient`. Returns `True` if downloaded successfully or `False` otherwise|
|`_set_logger`          |N/A            |Sets the module's logger, and silences dependent modules own's loggers.|
|`_connect_to_host`     |N/A            |Connects to the host via SSH. It also checks the latest time that the configuration file was modified. If it was within `DELTA_RE_CHECK_CONF` it re-loads the server configuration by reading the file. It returns a `SSHClient` object as well as a `SCPClient`.|

---

- Utilities functions

|Method             |Parameters                                 |Description|
|:-----             |:--------:                                 |:----------|
|`get_file_hash`    |`fn, hasher, blocksize=65536`              |Returns a file's (`fn`) hash string using one of `haser` function declared under `haslib` (by default SHA1)|
|`get_server_conf`  |`fn, def_hostname, def_username, def_port` |Reads a file (`fn`), which is the configuration file and retrieves sets of size 3 of configuration values: HOST, USERNAME, and PORT. If there is an error reading it. It returns the `def_*` values.|

**Notes:**
- The `blocksize` for retrieving the hash of a file is choosen to be `65536` because after research this is the value that generates the best performance.

### Player (`player.py`)

Separate process that plays (in an infinite loop) the current schedule's play-list.

- It constantly checks for a new schedule to play (5 seconds).

- It constantly checks for an urgent marquee that could override the schedule's marquee.
     - If the marquee content is an empty string. The Java program that displays the marquee will **not** get called.
- In the case of no schedule, it checks for future schedules (next 5 minutes), and if there is none it attempts to turn the HDMI out off.

#### Description of methods and constants:

|Constant               |Value              |Description|
|:-------               |:---:              |:----------|
|`HOME`                 |`"~/"`             |**Absolute** path to home|
|`DOWNLOAD_PATH`        |`"~/Downloads"`    |**Absolute** path to directory where files got downloaded|
|`WAIT_NO_SCHED`        |`15`               |Time in seconds to wait if there is no schedule to play|
|`WAIT_SAME_SCHED`      |`30`               |Time in seconds to wait if the schedule to play at this moment is the same as the being played|
|`MARQUEE_TEXT_FILE`    |`'tmp_marquee.txt'`|File name of the temporal file used to store a (UTF-8) marquee text|
|`tv_on`                |`False`            |Assume that the TV is off when `player` first runs|

---

##### Class: `player`

|Method                 |Parameters         |Description|
|:-----                 |:--------:         |:----------|
|`start_playing`        |N/A                |Gets the schedule that needs to be player currently, creates a playlist using `omxd`, and starts it. It checks every `SAME_SCHED_WAIT` seconds for a new schedule, if there is not it continues playing the current schedule. Also, if there is no schedule to play it waits for `NO_SCHED_WAIT` seconds until checking for a new schedule again.|
|`_add_playlist`        |`playlist`         |Adds a list of files (`playlist`) to `omxd` by performing an `A` (append) command to the `oxmd` daemon.|
|`_flush_omxd_playlist` |N/A                |Stops the playing playlist and removes all the files from the current playlist.|
|`_setup_omxd`          |N/A                |Tries to kill dangling instances of `omxplayer` or `omxd`, and sets up `omxdplayer` flags for a fresh start, and start `omxd` daemon.|
|`_display_marquee`     |`marquee_text=""`  |Calls `kill_marquee` and runs the `DisplayMarquee.jar` with the given marquee text by storing it in a temporal file that is then read by the Java program.|
|`_kill_marquee`        |N/A                |Kill any running instance of `Java` |

---

- Utilities functions

|Method             |Parameters         |Description|
|:-----             |:--------:         |:----------|
|`cd`               |`new_dir`          |Simulates use of `cd` bash command using `with` context|
|`turn_tv_on`       |N/A                |Sets the HDMI device to on|
|`turn_tv_off`      |N/A                |Sets the HDMI device to off|
|`exit_gracefully`  |N/A                |Method call `@atexit` to gracefully kill any instances of `omxplayer`|
|`sigusr1`          |`signum, frame`    |Handler of stop/sleep signal sent from `supervisor`. The signal interrupts sleep() calls, so the currently playing web or image asset is skipped. `omxplayer` is killed to skip any currently playing video assets.|

### Database handler/API (`db_manager.py`)

Module that acts as a intermediate between the database (`coe_client.db`) and different modules by providing an API to query the database. The current supported functions are:

|Constant               |Value                          |Description|
|:-------               |:---:                          |:----------|
|`DB_NAME`              |`'coe_client.db'`              |Database file name|
|`DB_ABS_PATH`          |`./db/DB_NAME`                 |**Absolute** path to database file|
|`DB_PATH`              |`'sqlite:///' + DB_ABS_PATH`   |Relative path to database file appending `sqlite3`|
|`DEF_INTERVAL`         |`0`                            |Default interval for files when query fails|
|`DEF_START_DT`         |`'1990-01-01 00:00:00'`        |Default start date & time when query fails|
|`DEF_END_DT`           |`'1990-01-01 00:00:00'`        |Default end date & time when query fails|
|`NOT_DOWNLOADED`       |`0`                            |Mask to define not downloaded files.|
|`DOWNLOADED`           |`1`                            |Mask to define (completely) downloaded files.|
|`DOWNLOADING`          |`2`                            |Mask to define downloading files.|

---

#### Class: `db_manager.py`

|Method                         |Parameters |Description|
|:-----                         |:--------: |:----------|
|`_connect`                     | N/A        | Connects to the `coe_client.db` database. |
|`save_marquee_to_db`           | `js`       | Stores the information from an urgent marquee `JSON` file into the database.|
|`save_sched_to_db`             | `js`       | Stores the information from a schedule `JSON` file into the database. |
|`is_sched_active`              | `sched_id` | Checks if a given schedule (id) `is_active` or no. Meaning that an schedule is ready to play and all its files have been successfully downloaded.|
|`flip_sched_is_active`         | `sched_id` | Flips the `is_active` column for a given schedule id (if it is 0 it flips it to 1 and vice versa)|
|`get_sorted_list_schedules`    | N/A | Returns list of schedules ids organized by their priority|
|`get_list_schedules`           | N/A | Returns list of schedules ids in the database|
|`get_sched_to_play`            | `curr_day`, `curr_datetime` | Returns the id of the schedule that needs to be played based on the `curr_day` and current date and time and also if the schedule is already active or not|
|`get_list_files_and_locations` | `sched_id=None` |Returns the list of location and file names for all the files for the given schedule, if called with only one argument it uses the latest added schedule|
|`get_list_files`               | `sched_id=None` |Returns the list of files for the given schedule, if called with only one argument it uses the latest added schedule|
|`get_list_files_locations`     | `sched_id=None` | Returns the list of location for all the files for the given schedule, if called with only one argument it uses the latest added schedule|
|`get_latest_sched_id`          | N/A | Calls `get_list_schedules` and gets the latest element of the list |
|`get_days_of_week`             | `sched_id=None` |Returns the list of days to play for the given schedule, if called with only one argument it uses the latest added schedule|
|`get_start_datetime`           | `sched_id=None` |Returns the the start time for the given schedule, if called with only one argument it uses the latest added schedule|
|`get_end_datetime`             | `sched_id=None` |Returns the the end time for the given schedule, if called with only one argument it uses the latest added schedule|
|`get_sched_marquee`            | `sched_id=None` |Returns the marquee text of a given schedule. If no schedule is given uses the current schedule|
|`set_active_schedules`         | N/A | Checks the downloaded files, if all the downloaded files for a schedule are downloaded sets its flag `is_downloaded` to True |
|`get_file_interval`            | `file_name` |Returns the interval of a given file name|
|`get_list_all_files`           | N/A |Returns list of all the files in the file table (for all schedules)|
|`get_file_type`                | `file_name` | Returns file type for the given file name (unique naming)|
|`is_file_downloaded_yet`       | `file_name` | Returns `true` if file was downloaded, given by download_yet column |
|`is_file_downloading`          | `file_name` | Returns `true` if file is being download (in progress), given by download_yet column |
|`set_downloaded_yet`           | `file_name`, `new_value` | Sets the `download_yet` value given by `new_value`|
|`_get_files_given_downloaded_yet_value` | `value` | Returns a list of files that match the given `download_yet` column with `value`|
|`get_files_to_download`        | N/A | Returns a list of all that should be downloaded (`downloaded_yet` column = 0 & 1) |
|`get_downloaded_files`         | N/A | Returns a list of all downloaded files (`downloaded_yet` column = 1)|
|`get_downloading_files`        | N/A | Returns a list of all downloading files (`downloaded_yet` column = 2)|
|`get_marquee_metadata`         | N/A | List of all the urgent marquees present in the marquee table |
|`get_urgent_marquee_to_play`   | `curr_datetime` | Queries the database for urgent marquee to play given that current time is between `start datetime` and `end datetime`. If there is more than one urgent marquee to play then it randomly chooses 1 by `LIMIT`ing the query to 1. If there is no such marquee then it returns an empty string. |
|`delete_expired_marquees`      | N/A | Deletes all the expired urgent marquees by looking at their end dates and comparing them to the current date |
|`delete_file`                  | `file_name` | Deletes a given file, indexed by its file name|
|`delete_expired_schedules`     | N/A | Deletes from the database all schedules that have expired. Expiration is defined if `end_date` (with the format `YYYY-MM-DD`) is less than the current date. Returns List of all deleted schedules that have expired. If no schedule has been deleted either due to an error or lack of schedules to deleted an empty list will be returned.|
|`delete_orphan_files`          | N/A | Deletes all the files that are not mapped in `file_sched`|
|`delete_orphans_schedules`     | N/A | Deletes all schedules that have not files|


##### `db_manager` utility functions
|Method                         |Parameters |Description|
|:-----                         |:--------: |:----------|
|`stop_player`|N/A|Stops the `player` using the `supervisord`|
|`resume_player`|N/A|Resumes the `player` using the `supervisord`|
|`write_unicode`|`text`,`charset='utf-8'`| Returns `text` encoded using `charset`|
|`read_unicode`|`text`,`charset='utf-8'`| Returns `text` by decoding the unicode using `charset`|

## Helper Modules/Scripts

### `clean_memory.py`

This modules is executed after every reboot. It checks the system's current free-space, and if it has passed a set threshold (25% of free-memory remaining), then it will delete the necessary (unused) file(s) to bring the free-space percentage below the threshold again. The order of deletion is given by the oldest files that are **not** being used by any current schedule.

- It also deletes any expired schedule(s) and marquee(s) from the database.

#### Notes:

- In the case that after removing all possible files (files not being used by any schedule) the free-space percentage is smaller than the threshold, it prompts an error message.
- In the extreme case that there is no enough memory to store a single file the `downloader` might have problems downloading files for new schedule(s). So any new schedule that tries to download files will fail and all its file will get removed from the database (after trying to download them *x*(5) times).

### `create_db.py`

Separate script that creates the initial schema of `coe_client.db` database. It handles three cases:

- If the `db/` directory does **not** exists, it creates it and the database file.
- If the `db/` directory exits but the database file does **not**, it creates it.
- If both `db/` directory and database file exits it terminates.

> This program gets called from the `setup.sh` script.

### `log_setter`

Simple and short module used to set a consistent logging format and level throughout all the modules.
